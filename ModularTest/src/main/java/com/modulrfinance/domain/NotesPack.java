package com.modulrfinance.domain;

import com.modulrfinance.types.Denomination;
import java.util.EnumMap;





/**
 * This class represents a collection of paper currency with notes of various denominations.
 *
 * It is used to represent both
 * the "vault" of the ATM: the entire collection of notes in the ATM machine
 * AND
 * the notes that the user receives in their withdrawal from the ATM.
 */
public class NotesPack {

    private final EnumMap<Denomination, Long> money;

    private NotesPack() {
        money = new EnumMap<>(Denomination.class);
        for (Denomination denomination : Denomination.valuesDescending()) {
            money.put(denomination, 0L);
        }
    }

    public static NotesPack createEmpty() {
        return new NotesPack();
    }

    public long get(Denomination denomination) {
        return money.get(denomination);
    }

    public NotesPack add(Denomination denomination, long quantity) {
        if (quantity < 0) {
            throw new IllegalArgumentException("Cannot add a negative quantity of notes.");
        }
        NotesPack newMoneyAsNotes = copy();
        newMoneyAsNotes.money.put(denomination, newMoneyAsNotes.money.get(denomination) + quantity);
        return newMoneyAsNotes;
    }

    public NotesPack add(NotesPack other) {
        NotesPack newMoneyAsNotes = copy();
        for (Denomination denomination : newMoneyAsNotes.money.keySet()) {
            newMoneyAsNotes = newMoneyAsNotes.add(denomination, other.get(denomination));
        }
        return newMoneyAsNotes;
    }

    public NotesPack remove(Denomination denomination, long quantity) {
        if (quantity < 0) {
            throw new IllegalArgumentException("Cannot remove a negative quantity of notes.");
        }
        NotesPack newMoneyAsNotes = copy();
        Long currentQuantity = newMoneyAsNotes.money.get(denomination);
        if (quantity > currentQuantity) {
            throw new IllegalArgumentException("Not enough notes in the pool to remove.");
        }
        newMoneyAsNotes.money.put(denomination, currentQuantity - quantity);
        return newMoneyAsNotes;
    }

    public NotesPack remove(NotesPack other) {
        NotesPack newMoneyAsNotes = copy();
        for (Denomination denomination : money.keySet()) {
            newMoneyAsNotes = newMoneyAsNotes.remove(denomination, other.get(denomination));
        }
        return newMoneyAsNotes;
    }

    private NotesPack copy() {
        NotesPack newMoneyAsNotes = createEmpty();
        for (Denomination denomination : money.keySet()) {
            newMoneyAsNotes.money.put(denomination, get(denomination));
        }
        return newMoneyAsNotes;
    }
}
