package com.modulrfinance.exception;

import static java.lang.String.format;

public class AccountNotFoundException extends Exception {

    public AccountNotFoundException(String accountNumber) {
        super(format("Account [%s] has not been found.", accountNumber));
    }
}
