package com.modulrfinance.exception;

public class NotEnoughNotesException extends Exception {

    public NotEnoughNotesException() {
        super("Not enough notes in the ATM.");
    }
}
