package com.modulrfinance.types;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import static java.util.stream.Collectors.toList;

public enum Denomination {
    FIVE(5.00),
    TEN(10.00),
    TWENTY(20.00),
    FIFTY(50.00);

    private final double value;

    Denomination(double value) {
        this.value = value;
    }

    public double value() {
        return value;
    }

    public static List<Denomination> valuesDescending() {
        return Arrays.stream(Denomination.values())
                .sorted(Comparator.reverseOrder())
                .collect(toList());
    }
}
