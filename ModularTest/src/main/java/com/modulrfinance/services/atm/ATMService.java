package com.modulrfinance.services.atm;

import com.modulrfinance.exception.AccountNotFoundException;
import com.modulrfinance.exception.InsufficientFundsException;
import com.modulrfinance.exception.NotEnoughNotesException;
import com.modulrfinance.services.account.AccountService;
import com.modulrfinance.types.Denomination;
import com.modulrfinance.domain.NotesPack;

public class ATMService {

    private AccountService accountService;
    private NotesPack vault;

    public void init(AccountService accountService, NotesPack vault) {
        this.accountService = accountService;
        this.vault = vault;
    }

    public double checkBalance(String accountNumber) throws AccountNotFoundException {
        double balance = accountService.checkBalance(accountNumber);
        return balance;
    }

    public void replenish(NotesPack moneyAsNotes) {
        vault = vault.add(moneyAsNotes);
    }

    public NotesPack withdraw(String accountNumber, double amount) throws AccountNotFoundException, InsufficientFundsException, NotEnoughNotesException {
        NotesPack withdrawalPool = prepareMoney(amount);
        accountService.withdraw(accountNumber, amount);
        vault = vault.remove(withdrawalPool);
        return withdrawalPool;
    }

    private NotesPack prepareMoney(double amount) throws NotEnoughNotesException {
        NotesPack moneyAsNotes = prepareMoneyStep(amount, vault);
        if (moneyAsNotes == null) {
            throw new NotEnoughNotesException();
        }
        return moneyAsNotes;
    }

    private NotesPack prepareMoneyStep(double amount, NotesPack remainingVault) {
        if (amount == 0) {
            return NotesPack.createEmpty();
        }
        if (amount < 0) {
            return null;
        }
        for (Denomination denomination : Denomination.valuesDescending()) {
            if (remainingVault.get(denomination) > 0) {
                double remainingAmount = amount - denomination.value();
                NotesPack moneyAsNotes = prepareMoneyStep(remainingAmount, remainingVault.remove(denomination, 1));
                if (moneyAsNotes != null) {
                    return moneyAsNotes.add(denomination, 1);
                }
            }
        }
        return null;
    }
}
