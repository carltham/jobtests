package com.modulrfinance.services.account;

import com.modulrfinance.exception.AccountNotFoundException;
import com.modulrfinance.exception.InsufficientFundsException;
import static java.lang.String.format;
import java.util.HashMap;
import java.util.Map;

public class AccountService {

    private Map<String, Double> accounts;

    public AccountService() {
        accounts = new HashMap<>();
    }

    public double checkBalance(String accountNumber) throws AccountNotFoundException {
        if (!accounts.containsKey(accountNumber)) {
            throw new AccountNotFoundException(accountNumber);
        }
        double balance = accounts.get(accountNumber);
        return balance;
    }

    public void withdraw(String accountNumber, double amount) throws AccountNotFoundException, InsufficientFundsException {
        if (!accounts.containsKey(accountNumber)) {
            throw new AccountNotFoundException(accountNumber);
        }
        double currentBalance = accounts.get(accountNumber);
        double newBalance = currentBalance - amount;
        if (newBalance < 0) {
            throw new InsufficientFundsException();
        }
        accounts.put(accountNumber, newBalance);
    }

    public void createAccount(String accountNumber, double balance) {
        if (accounts.containsKey(accountNumber)) {
            throw new IllegalArgumentException(format("Account [%s] already exists.", accountNumber));
        }
        if (balance < 0) {
            throw new IllegalArgumentException("Cannot create account with the negative balance.");
        }
        accounts.put(accountNumber, balance);
    }
}
