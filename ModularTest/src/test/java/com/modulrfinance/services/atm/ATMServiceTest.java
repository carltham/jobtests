package com.modulrfinance.services.atm;

import com.modulrfinance.technicaltest.utils.TestVerificator;
import com.modulrfinance.domain.NotesPack;
import com.modulrfinance.exception.AccountNotFoundException;
import com.modulrfinance.exception.InsufficientFundsException;
import com.modulrfinance.exception.NotEnoughNotesException;
import com.modulrfinance.technicaltest.utils.AccountTestHelper;
import static com.modulrfinance.technicaltest.utils.AccountTestHelper._DEFAULT_ACCOUNT;
import com.modulrfinance.technicaltest.utils.AtmServiceTestHelper;
import static com.modulrfinance.types.Denomination.FIFTY;
import static com.modulrfinance.types.Denomination.FIVE;
import static com.modulrfinance.types.Denomination.TEN;
import static com.modulrfinance.types.Denomination.TWENTY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import org.junit.Before;
import org.junit.Test;

public class ATMServiceTest {

    private AccountTestHelper accountTestHelper;
    private AtmServiceTestHelper atmServiceTestHelper;

    @Before
    public void setup() {
        accountTestHelper = new AccountTestHelper();
        atmServiceTestHelper = new AtmServiceTestHelper();
    }

    @Test
    public void checksAccountBalance() throws AccountNotFoundException {
        // given

        accountTestHelper.createDefaultAccount(2738.59);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), NotesPack.createEmpty());

        // when
        double balance = atmServiceTestHelper.checkBalance();

        // then
        assertThat(balance).isEqualTo(2738.59);
    }

    @Test
    public void throwsExceptionWhenCheckingNonexistentAccountBalance() {
        // given
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), NotesPack.createEmpty());

        // when
        Throwable thrown = catchThrowable(() -> atmServiceTestHelper.checkBalance());

        // then
        assertThat(thrown)
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage("Account [" + _DEFAULT_ACCOUNT + "] has not been found.");
    }

    @Test
    public void withdrawsMoney() throws AccountNotFoundException, InsufficientFundsException, NotEnoughNotesException {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIFTY, 1)
                .add(TWENTY, 3);

        accountTestHelper.createDefaultAccount(2738.59);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), notesPack);

        // when
        NotesPack withdrawnMoney = atmServiceTestHelper.withdraw(60.00);

        // then
        TestVerificator.verifyExactly(withdrawnMoney, new Object[][]{{TWENTY, 3}});

        // and
        assertThat(atmServiceTestHelper.checkBalance()).isEqualTo(2678.59);

        Throwable throwable = catchThrowable(() -> atmServiceTestHelper.withdraw(20.00));
        assertThat(throwable).isInstanceOf(NotEnoughNotesException.class);
    }

    @Test
    public void withdrawsMoneyWithMinimalNumberOfNotesDispensed() throws AccountNotFoundException, InsufficientFundsException, NotEnoughNotesException {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(TWENTY, 6)
                .add(TEN, 2)
                .add(FIFTY, 2);

        accountTestHelper.createDefaultAccount(2738.59);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), notesPack);

        // when
        NotesPack withdrawnMoney = atmServiceTestHelper.withdraw(120.00);

        // then
        TestVerificator.verifyExactly(withdrawnMoney, new Object[][]{{TWENTY, 1}, {FIFTY, 2}});
    }

    @Test
    public void prioritiesFivesWhenDispensingMoney() throws AccountNotFoundException, InsufficientFundsException, NotEnoughNotesException {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 10)
                .add(FIFTY, 1);

        accountTestHelper.createDefaultAccount(2738.59);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), notesPack);

        // when
        NotesPack withdrawnMoney = atmServiceTestHelper.withdraw(50.00);

        // then
        TestVerificator.verifyExactly(withdrawnMoney, new Object[][]{{FIFTY, 1}});
    }

    @Test
    public void fiveNoteRuleDoesNotBreakTheMoneyWithdrawal() throws AccountNotFoundException, InsufficientFundsException, NotEnoughNotesException {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 1)
                .add(TWENTY, 1);

        accountTestHelper.createDefaultAccount(2738.59);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), notesPack);

        // when
        NotesPack withdrawnMoney = atmServiceTestHelper.withdraw(20.00);

        // then
        TestVerificator.verifyExactly(withdrawnMoney, new Object[][]{{TWENTY, 1}});
    }

    @Test
    public void fiveNoteRuleAndMinimalNotesRuleWorkTogether() throws AccountNotFoundException, InsufficientFundsException, NotEnoughNotesException {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 5)
                .add(TWENTY, 1);

        accountTestHelper.createDefaultAccount(2738.59);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), notesPack);

        // when
        NotesPack withdrawnMoney = atmServiceTestHelper.withdraw(25.00);

        // then
        TestVerificator.verifyExactly(withdrawnMoney, new Object[][]{{TWENTY, 1}, {FIVE, 1}});
    }

    @Test
    public void throwsExceptionWhenWithdrawingMoneyFromNonexistentAccount() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(TWENTY, 3)
                .add(TEN, 1)
                .add(FIFTY, 1);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), notesPack);

        // when
        Throwable thrown = catchThrowable(() -> atmServiceTestHelper.withdraw(60.00));

        // then
        assertThat(thrown)
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage("Account [" + _DEFAULT_ACCOUNT + "] has not been found.");
    }

    @Test
    public void throwsExceptionWhenNotEnoughNotesInATM() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(TWENTY, 2);

        accountTestHelper.createDefaultAccount(2738.59);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), notesPack);

        // when
        Throwable thrown = catchThrowable(() -> atmServiceTestHelper.withdraw(60.00));

        // then
        assertThat(thrown)
                .isInstanceOf(NotEnoughNotesException.class);
    }

    @Test
    public void replenishesMoney() throws AccountNotFoundException, InsufficientFundsException, NotEnoughNotesException {
        // given

        accountTestHelper.createDefaultAccount(2738.59);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), NotesPack.createEmpty());
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 4)
                .add(TEN, 3)
                .add(TWENTY, 2)
                .add(FIFTY, 1);

        // when
        atmServiceTestHelper.replenish(notesPack);

        // then
        atmServiceTestHelper.withdraw(140.00);
        TestVerificator.verifyExactly(notesPack, new Object[][]{{FIVE, 4}, {TEN, 3}, {TWENTY, 2}, {FIFTY, 1}});

        // and
        assertThat(catchThrowable(() -> atmServiceTestHelper.withdraw(50.00))).isInstanceOf(NotEnoughNotesException.class);
        assertThat(catchThrowable(() -> atmServiceTestHelper.withdraw(20.00))).isInstanceOf(NotEnoughNotesException.class);
    }

    @Test
    public void throwsExceptionWhenInsufficientFunds() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIFTY, 1);

        accountTestHelper.createDefaultAccount(49.99);
        atmServiceTestHelper.init(accountTestHelper.getAccountService(), notesPack);

        // when
        Throwable throwable = catchThrowable(() -> atmServiceTestHelper.withdraw(50.00));

        // and
        assertThat(throwable).isInstanceOf(InsufficientFundsException.class);
    }

}
