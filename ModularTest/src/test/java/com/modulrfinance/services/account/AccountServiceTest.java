package com.modulrfinance.services.account;

import com.modulrfinance.exception.AccountNotFoundException;
import com.modulrfinance.exception.InsufficientFundsException;
import com.modulrfinance.technicaltest.utils.AccountTestHelper;
import static com.modulrfinance.technicaltest.utils.AccountTestHelper._DEFAULT_ACCOUNT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import org.junit.Before;
import org.junit.Test;

public class AccountServiceTest {

    private AccountTestHelper accountTestHelper;

    @Before
    public void setup() {
        accountTestHelper = new AccountTestHelper();
    }

    @Test
    public void checksAccountBalance() throws AccountNotFoundException {
        // given

        accountTestHelper.createDefaultAccount(2738.59);

        // when
        double balance = accountTestHelper.checkBalance();

        // then
        assertThat(balance).isEqualTo(2738.59);
    }

    @Test
    public void throwsExceptionWhenCheckingNonexistentAccountBalance() {
        // given

        // when
        Throwable thrown = catchThrowable(() -> accountTestHelper.checkBalance());

        // then
        assertThat(thrown)
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage("Account [" + _DEFAULT_ACCOUNT + "] has not been found.");
    }

    @Test
    public void withdrawsMoney() throws AccountNotFoundException, InsufficientFundsException {
        // given

        accountTestHelper.createDefaultAccount(2738.59);

        // when
        accountTestHelper.withdraw(11.90);

        // then
        assertThat(accountTestHelper.checkBalance()).isEqualTo(2726.69);
    }

    @Test
    public void throwsExceptionWhenWithdrawingMoneyFromNonexistentAccount() {
        // given

        // when
        Throwable thrown = catchThrowable(() -> accountTestHelper.withdraw(11.90));

        // then
        assertThat(thrown)
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage("Account [" + _DEFAULT_ACCOUNT + "] has not been found.");
    }

    @Test
    public void throwsExceptionWhenInsufficientFunds() {
        // given

        accountTestHelper.createDefaultAccount(99.99);

        // when
        Throwable thrown = catchThrowable(() -> accountTestHelper.withdraw(100.00));

        // then
        assertThat(thrown)
                .isInstanceOf(InsufficientFundsException.class);
    }

    @Test
    public void throwsExceptionWhenCreatingAccountThatAlreadyExists() {
        // given

        accountTestHelper.createDefaultAccount(2738.59);

        // when
        Throwable thrown = catchThrowable(() -> accountTestHelper.createDefaultAccount(2738.59));

        // then
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Account [" + _DEFAULT_ACCOUNT + "] already exists.");
    }

    @Test
    public void throwsExceptionWhenCreatingAccountWithNegativeBalance() {
        // given

        // when
        Throwable thrown = catchThrowable(() -> accountTestHelper.createDefaultAccount(-100.00));

        // then
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Cannot create account with the negative balance.");
    }

}
