package com.modulrfinance.domain;

import com.modulrfinance.technicaltest.utils.TestVerificator;
import static com.modulrfinance.types.Denomination.FIFTY;
import static com.modulrfinance.types.Denomination.FIVE;
import static com.modulrfinance.types.Denomination.TEN;
import static com.modulrfinance.types.Denomination.TWENTY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import org.junit.Test;

public class NotesPackTest {

    @Test
    public void addsNotesToMoneyPool() {
        // given
        NotesPack notesPack = NotesPack.createEmpty();

        // when
        notesPack = notesPack.add(FIVE, 1)
                .add(TEN, 2)
                .add(TWENTY, 3)
                .add(FIFTY, 4);

        // then
        TestVerificator.verifyExactly(notesPack, new Object[][]{{FIVE, 1}, {TEN, 2}, {TWENTY, 3}, {FIFTY, 4}});
    }

    @Test
    public void cannotAddNegativeQuantityOfNotes() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 10);

        // when
        Throwable thrown = catchThrowable(() -> notesPack.add(FIVE, -1));

        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        TestVerificator.verifyExactly(notesPack, new Object[][]{{FIVE, 10}});
    }

    @Test
    public void removesNotesFromMoneyPool() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 10);

        // when
        notesPack = notesPack.remove(FIVE, 3);

        // then
        TestVerificator.verifyExactly(notesPack, new Object[][]{{FIVE, 7}});
    }

    @Test
    public void cannotRemoveNegativeQuantityOfNotes() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 10);

        // when
        Throwable thrown = catchThrowable(() -> notesPack.remove(FIVE, -1));

        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        TestVerificator.verifyExactly(notesPack, new Object[][]{{FIVE, 10}});
    }

    @Test
    public void cannotRemoveMoreThanCurrentQuantityOfNotes() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 10);

        // when
        Throwable thrown = catchThrowable(() -> notesPack.remove(FIVE, 15));

        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        TestVerificator.verifyExactly(notesPack, new Object[][]{{FIVE, 10}});
    }

    @Test
    public void removesMoneyPoolFromSourceMoneyPool() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 10)
                .add(TEN, 3);

        NotesPack anotherMoneyAsNotes = NotesPack.createEmpty()
                .add(FIVE, 3)
                .add(TEN, 1);

        // when
        notesPack = notesPack.remove(anotherMoneyAsNotes);

        // then
        TestVerificator.verifyExactly(notesPack, new Object[][]{{FIVE, 7}, {TEN, 2}});
    }

    @Test
    public void cannotRemoveMoneyPoolFromSourceMoneyPoolIfItDoesNotContainCorrespondingNotes() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(FIVE, 10);

        NotesPack anotherMoneyAsNotes = NotesPack.createEmpty()
                .add(TWENTY, 1);

        // when
        Throwable thrown = catchThrowable(() -> notesPack.remove(anotherMoneyAsNotes));

        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void addsAnotherMoneyPool() {
        // given
        NotesPack notesPack = NotesPack.createEmpty()
                .add(TWENTY, 3)
                .add(FIFTY, 4);

        NotesPack anotherMoneyAsNotes = NotesPack.createEmpty()
                .add(FIVE, 1)
                .add(TEN, 2)
                .add(TWENTY, 3);

        // when
        notesPack = notesPack.add(anotherMoneyAsNotes);

        // then
        TestVerificator.verifyExactly(notesPack, new Object[][]{{FIVE, 1}, {TEN, 2}, {TWENTY, 6}, {FIFTY, 4}});
    }
}
