/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modulrfinance.technicaltest.utils;

import com.modulrfinance.exception.AccountNotFoundException;
import com.modulrfinance.exception.InsufficientFundsException;
import com.modulrfinance.exception.NotEnoughNotesException;
import com.modulrfinance.services.account.AccountService;
import com.modulrfinance.services.atm.ATMService;
import com.modulrfinance.domain.NotesPack;
import static com.modulrfinance.technicaltest.utils.AccountTestHelper._DEFAULT_ACCOUNT;

/**
 *
 * @author carl
 */
public class AtmServiceTestHelper {

    private final ATMService aTMService;

    public AtmServiceTestHelper() {
        this.aTMService = new ATMService();
    }

    public double checkBalance() throws AccountNotFoundException {
        double balance = aTMService.checkBalance(_DEFAULT_ACCOUNT);
        return balance;
    }

    public void init(AccountService accountService, NotesPack vault) {
        aTMService.init(accountService, vault);
    }

    public NotesPack withdraw(double amount) throws AccountNotFoundException, InsufficientFundsException, NotEnoughNotesException {
        NotesPack notes = aTMService.withdraw(_DEFAULT_ACCOUNT, amount);
        return notes;
    }

    public void replenish(NotesPack notes) {
        aTMService.replenish(notes);
    }

}
