/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modulrfinance.technicaltest.utils;

import com.modulrfinance.exception.AccountNotFoundException;
import com.modulrfinance.exception.InsufficientFundsException;
import com.modulrfinance.services.account.AccountService;

/**
 *
 * @author carl
 */
public class AccountTestHelper {

    public static final String _DEFAULT_ACCOUNT = "01001";
    private final AccountService accountService;

    public AccountTestHelper() {
        accountService = new AccountService();
    }

    public void createDefaultAccount(double amount) {
        accountService.createAccount(_DEFAULT_ACCOUNT, amount);
    }

    public double checkBalance() throws AccountNotFoundException {
        double balance = accountService.checkBalance(_DEFAULT_ACCOUNT);
        return balance;
    }

    public void withdraw(double amount) throws AccountNotFoundException, InsufficientFundsException {
        accountService.withdraw(_DEFAULT_ACCOUNT, amount);
    }

    public AccountService getAccountService() {
        return accountService;
    }

}
