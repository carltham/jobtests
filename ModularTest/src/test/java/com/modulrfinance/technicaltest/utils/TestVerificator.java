/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modulrfinance.technicaltest.utils;

import com.modulrfinance.domain.NotesPack;
import com.modulrfinance.types.Denomination;
import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * @author carl
 */
public class TestVerificator {

    public static void verifyExactly(NotesPack vault, Object[][] denominationPairs) {
        for (Denomination denomination : Denomination.valuesDescending()) {
            int nrOfNotes = 0;
            for (Object[] denominationPair : denominationPairs) {
                Denomination verifyingDenomination = Denomination.valueOf(denominationPair[0].toString());
                if (verifyingDenomination == denomination) {
                    nrOfNotes = (int) denominationPair[1];
                }
            }
            assertThat(vault.get(denomination)).isEqualTo(nrOfNotes);
        }
    }

}
