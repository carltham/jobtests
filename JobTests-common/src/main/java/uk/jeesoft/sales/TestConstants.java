/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.sales;

/**
 *
 * @author carl
 */
public interface TestConstants {

    public String EXPECTED_OUTPUT_NO_OFFERS = "\nSubtotal: £1.30\n"
            + "(no offers available)\n"
            + "Total: £1.30\n";
    public String EXPECTED_OUTPUT_WITH_OFFERS = "\nSubtotal: £3.10\n"
            + "Apples 10.0% off: -10p\n"
            + "Total: £3.00\n";

}
