/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waracle.cakemgr.api;

import java.io.PrintWriter;
import java.lang.reflect.Method;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;

/**
 *
 * @author carl
 */
public class CakesServletTest_using_assist {

    private static final String DON_T_CARE = "don't care";
    private static final String SEND_ERROR = "sendError";
    private static final String GET_PARAMETER = "getParameter";

    private CakesServlet instance;

    @Before
    public void setUp() throws ServletException {
        instance = new CakesServlet();
        instance.init();
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void should_Set_ResourceNotFound_If_Id_Is_Null() throws Exception {

        // request object that returns null for getParameter("id") method.
        HttpServletRequest request = (HttpServletRequest) createObject(new Class[]{HttpServletRequest.class},
                new MethodHandler() {
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
                if (thisMethod.getName().compareToIgnoreCase(GET_PARAMETER) == 0) {
                    return ((String) args[0]).compareToIgnoreCase("id") == 0 ? null : "oops";
                }
                return DON_T_CARE;
            }
        }
        );

        // Response object that asserts that sendError arg is resource not found: 404.
        HttpServletResponse response = (HttpServletResponse) createObject(new Class[]{HttpServletResponse.class},
                new MethodHandler() {
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
                if (thisMethod.getName().compareTo(SEND_ERROR) == 0) {
                    MatcherAssert.assertThat((Integer) args[0], IsEqual.equalTo(HttpServletResponse.SC_NOT_FOUND));
                }
                PrintWriter printWriter = mock(PrintWriter.class);
                return printWriter;
            }
        }
        );

        instance.doGet(request, response);
    }

    /**
     * Create Object based on interface.
     * <p>
     * Just to remove duplicate code in
     * should_Return_ResourceNotFound_If_Id_Is_Null test.
     *
     * @param interfaces array of T interfaces
     * @param mh MethodHandler
     * @return Object
     * @throws Exception
     */
    private <T> Object createObject(T[] interfaces, MethodHandler mh) throws Exception {
        ProxyFactory factory = new ProxyFactory();
        factory.setInterfaces((Class<?>[]) interfaces); // hmmm.        
        return factory.create(new Class[0], new Object[0], mh);
    }
}
