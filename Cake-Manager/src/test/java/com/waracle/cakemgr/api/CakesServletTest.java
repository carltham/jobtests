/* * To change this license header, choose License Headers in Project Properties. * To change this template file, choose Tools | Templates * and open the template in the editor. */package com.waracle.cakemgr.api;

import com.waracle.cakemgr.test.utils.TestPrintWriter;
import com.waracle.cakemgr.db.exceptions.TooManyResultsException;
import com.waracle.cakemgr.test.exceptions.ExpectedTestException;
import com.waracle.cakemgr.test.utils.RecordingLogger;
import com.waracle.cakemgr.utils.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.stream.Collector;
import java.util.stream.Stream;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * * * @author carl
 */
public class CakesServletTest {

    private CakesServlet instance;
    private static TestDatabaseUtility testDatabaseUtility = new TestDatabaseUtility();

    @BeforeClass
    public static void init() throws IOException, TooManyResultsException {
        testDatabaseUtility.init();
    }

    @Before
    public void setUp() {

        instance = new CakesServlet();
        instance.setDatabaseUtility(testDatabaseUtility);
    }

    @After
    public void destroy() {

        instance.destroy();
    }

    @AfterClass
    public static void cleanup() {

        testDatabaseUtility.shutdownProperly();
    }

    @Test
    public void testInit() throws Exception {

        instance.init();
    }

    @Test
    public void testDoPost() throws Exception {
        String testJsonString = "{\"name\":\"Rabbit Cake\",\"description\":\"mmmmm\""
                + ",\"imageURL\":\"https://caringforpets.net/wp-content/uploads/2018/08/Holland-Lop-rabbit-image.jpg\"}";

        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        Logger recordingLogger = new RecordingLogger(new StringBuilder());
        Writer mockWriter = mock(Writer.class);
        TestPrintWriter testPrintWriter = new TestPrintWriter(mockWriter);
        BufferedReader mockReader = mock(BufferedReader.class);
        Stream<String> mockLines = mock(Stream.class);

        instance.setLogger(recordingLogger);

        when(req.getReader()).thenReturn(mockReader);
        when(mockReader.lines()).thenReturn(mockLines);
        when(mockLines.collect(any(Collector.class)))
                .thenReturn(testJsonString);
        when(resp.getWriter()).thenReturn(testPrintWriter);

        instance.doPost(req, resp);
        assertThat(recordingLogger.toString())
                .isEqualTo("");
        assertThat(testPrintWriter.getPrintedString()).isEqualTo(
                "{\"id\":5,\"name\":\"Rabbit Cake\",\"description\":\"mmmmm\","
                + "\"imageURL\":\"https://caringforpets.net/wp-content/uploads/2018/08/Holland-Lop-rabbit-image.jpg\"}");
    }

    @Test
    public void testDestroy() {

        instance.destroy();
    }

    @Test
    public void testRedirectIfBrowser_NonBrowser_Request() throws Exception {

        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        Logger recordingLogger = new RecordingLogger(new StringBuilder());
        Writer mockWriter = mock(Writer.class);
        TestPrintWriter testPrintWriter = new TestPrintWriter(mockWriter);

        instance.setLogger(recordingLogger);

        when(req.getHeader("Accept")).thenReturn("nonsence");
        when(req.getHeader("user-agent")).thenReturn("I guess it's you?");
        when(resp.getWriter()).thenReturn(testPrintWriter);

        instance.doGet(req, resp);
        assertThat(recordingLogger.toString())
                .isEqualTo(
                        "class com.waracle.cakemgr.api.CakesServlet"
                        + "=>Non Browser Request Detected");
        assertThat(testPrintWriter.getPrintedString()).isEqualTo(
                "[{\"id\":0,\"name\":\"Lemon cheesecake\","
                + "\"description\":\"A cheesecake made of lemon\","
                + "\"imageURL\":\"https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg\"},"
                + "{\"id\":1,\"name\":\"victoria sponge\","
                + "\"description\":\"sponge with jam\","
                + "\"imageURL\":\"http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg\"},"
                + "{\"id\":2,\"name\":\"Carrot cake\",\"description\":\"Bugs bunnys favourite\",\"imageURL\":\"http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg\"},"
                + "{\"id\":3,\"name\":\"Banana cake\",\"description\":\"Donkey kongs favourite\",\"imageURL\":\"http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg\"},"
                + "{\"id\":4,\"name\":\"Birthday cake\",\"description\":\"a yearly treat\",\"imageURL\":\"http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg\"}]");
    }

    @Test(expected = ExpectedTestException.class)
    public void testRedirectIfBrowser_Browser_Request() throws Exception {

        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        Logger recordingLogger = new RecordingLogger(new StringBuilder());
        ServletConfig mockConfig = mock(ServletConfig.class);
        ServletContext mockServletContext = mock(ServletContext.class);
        RequestDispatcher mockRequestDispatcher = mock(RequestDispatcher.class);
        instance.init(mockConfig);
        instance.setLogger(recordingLogger);

        when(req.getHeader("Accept")).thenReturn("text/html");
        when(req.getHeader("user-agent")).thenReturn("I guess it's you?");
        when(mockConfig.getServletContext()).thenReturn(mockServletContext);
        when(mockServletContext.getRequestDispatcher(any(String.class))).
                thenReturn(mockRequestDispatcher);
        doThrow(new ExpectedTestException()).when(mockRequestDispatcher).forward(any(ServletRequest.class), any(ServletResponse.class));

        instance.redirectIfBrowser(req, resp);
        fail("Should nevere reach here");
    }
}
