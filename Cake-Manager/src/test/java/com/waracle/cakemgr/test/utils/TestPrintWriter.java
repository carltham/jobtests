/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waracle.cakemgr.test.utils;

import java.io.PrintWriter;
import java.io.Writer;

/**
 *
 * @author carl
 */
public class TestPrintWriter extends PrintWriter {

    private String printedString;

    public TestPrintWriter(Writer out) {
        super(out);
    }

    @Override
    public void print(char c) {
        super.print(printedString);
    }

    @Override
    public void println(String newPrintedString) {
        printedString = newPrintedString;
    }

    public String getPrintedString() {
        return printedString;
    }
}
