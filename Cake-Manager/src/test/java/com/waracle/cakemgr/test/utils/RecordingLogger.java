/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waracle.cakemgr.test.utils;

import com.waracle.cakemgr.utils.Logger;

/**
 *
 * @author carl
 */
public class RecordingLogger extends Logger {

    private final StringBuilder stringbuilder;

    public RecordingLogger(StringBuilder newStringBuilder) {
        this.stringbuilder = newStringBuilder;
    }

    @Override
    public void log(Class<?> callingClass, Object object) {
        buildRow(callingClass, object);
    }

    @Override
    public void log(Class<?> callingClass, String message) {
        buildRow(callingClass, message);
    }

    @Override
    public void critical(Class<?> callingClass, Object object) {
        buildRow(callingClass, object);
    }

    @Override
    public void critical(Class<?> callingClass, String message) {
        buildRow(callingClass, message);
    }

    @Override
    public String toString() {
        return stringbuilder.toString();
    }

    private void buildRow(Class<?> callingClass, Object object) {
        stringbuilder.append(callingClass);
        stringbuilder.append("=>");
        stringbuilder.append(object);
    }

}
