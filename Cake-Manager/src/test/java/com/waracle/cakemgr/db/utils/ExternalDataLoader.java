/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waracle.cakemgr.db.utils;

import com.waracle.cakemgr.utils.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 *
 * @author carl
 */
public class ExternalDataLoader extends Logger {

    private String getExternalJsonData() throws IOException {
        log(getClass(), "downloading cake json array");
        URL dataURL = new URL(
                "https://gist.githubusercontent.com"
                + "/hart88/198f29ec5114a3ec3460/raw/"
                + "8dd19a88f9b8d24c23d9960f3300d0c917a4f07c"
                + "/cake.json"
        );
        InputStream dataStream = dataURL.openStream();
        BufferedReader dataReader = new BufferedReader(new InputStreamReader(dataStream));
        StringBuilder buffer = new StringBuilder();
        try {
            String line = dataReader.readLine();
            while (line != null) {
                buffer.append(line);
                line = dataReader.readLine();
            }
        } finally {
            dataReader.close();
        }
        String jsonString = buffer.toString();
        log(getClass(), jsonString);
        return jsonString;
    }

}
