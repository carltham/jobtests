/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waracle.cakemgr.db.entities;

/**
 *
 * @author carl
 */
public interface IEntity {

    public Long getId();

    public void setId(Long id);

    public String getName();

    public void setName(Object name);

}
