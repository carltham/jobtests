/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waracle.cakemgr.db.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.db.entities.Cake;
import com.waracle.cakemgr.external.ImportedCake;
import com.waracle.cakemgr.utils.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carl
 */
public class DataLoader extends Logger {

    private static final String DOWNLOADED_JSON_ARRAY_STRING
            = "[{\"title\":\"Lemon cheesecake\",\"desc\":\"A cheesecake made of lemon\",\"image\":\"https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg\"},{\"title\":\"victoria sponge\",\"desc\":\"sponge with jam\",\"image\":\"http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg\"},{\"title\":\"Carrot cake\",\"desc\":\"Bugs bunnys favourite\",\"image\":\"http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg\"},{\"title\":\"Banana cake\",\"desc\":\"Donkey kongs favourite\",\"image\":\"http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg\"},{\"title\":\"Birthday cake\",\"desc\":\"a yearly treat\",\"image\":\"http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg\"},{\"title\":\"Lemon cheesecake\",\"desc\":\"A cheesecake made of lemon\",\"image\":\"https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg\"},{\"title\":\"victoria sponge\",\"desc\":\"sponge with jam\",\"image\":\"http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg\"},{\"title\":\"Carrot cake\",\"desc\":\"Bugs bunnys favourite\",\"image\":\"http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg\"},{\"title\":\"Banana cake\",\"desc\":\"Donkey kongs favourite\",\"image\":\"http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg\"},{\"title\":\"Birthday cake\",\"desc\":\"a yearly treat\",\"image\":\"http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg\"},{\"title\":\"Lemon cheesecake\",\"desc\":\"A cheesecake made of lemon\",\"image\":\"https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg\"},{\"title\":\"victoria sponge\",\"desc\":\"sponge with jam\",\"image\":\"http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg\"},{\"title\":\"Carrot cake\",\"desc\":\"Bugs bunnys favourite\",\"image\":\"http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg\"},{\"title\":\"Banana cake\",\"desc\":\"Donkey kongs favourite\",\"image\":\"http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg\"},{\"title\":\"Birthday cake\",\"desc\":\"a yearly treat\",\"image\":\"http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg\"},{\"title\":\"Lemon cheesecake\",\"desc\":\"A cheesecake made of lemon\",\"image\":\"https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg\"},{\"title\":\"victoria sponge\",\"desc\":\"sponge with jam\",\"image\":\"http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg\"},{\"title\":\"Carrot cake\",\"desc\":\"Bugs bunnys favourite\",\"image\":\"http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg\"},{\"title\":\"Banana cake\",\"desc\":\"Donkey kongs favourite\",\"image\":\"http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg\"},{\"title\":\"Birthday cake\",\"desc\":\"a yearly treat\",\"image\":\"http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg\"}]";

    public List<Cake> init() throws IOException {
        String jsonString = DOWNLOADED_JSON_ARRAY_STRING;

        log(getClass(), "Parsing cake json array");
        ObjectMapper mapper = new ObjectMapper();

        List<ImportedCake> importedCakesList = mapper.readValue(jsonString, mapper.getTypeFactory()
                .constructCollectionType(List.class, ImportedCake.class));
        List<Cake> cakesList = new ArrayList<>();
        importedCakesList.forEach(importedCake -> cakesList.add(new Cake(importedCake)));
        return cakesList;
    }

}
