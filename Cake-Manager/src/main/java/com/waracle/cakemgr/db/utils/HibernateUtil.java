package com.waracle.cakemgr.db.utils;

import com.waracle.cakemgr.utils.Logger;
import java.net.URL;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil extends Logger {

    private SessionFactory sessionFactory;

    private SessionFactory buildSessionFactory() {
            if (sessionFactory == null) {
                URL resourceUrl = HibernateUtil.class.getResource("/hibernate.cfg.xml");
                Configuration configuration = new Configuration().configure(resourceUrl);
                StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
                serviceRegistryBuilder.applySettings(configuration.getProperties());
                ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            }
            return sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            buildSessionFactory();
        }
        return sessionFactory;
    }

}
