/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waracle.cakemgr.db.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.db.entities.Cake;
import com.waracle.cakemgr.db.entities.IEntity;
import com.waracle.cakemgr.db.exceptions.TooManyResultsException;
import com.waracle.cakemgr.utils.Logger;
import java.io.IOException;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;

/**
 *
 * @author carl
 */
public class DatabaseUtility extends Logger {

    private static HibernateUtil hibernateUtil = new HibernateUtil();
    private DataLoader dataLoader;

    public void addEntity(Cake cake) throws TooManyResultsException {
        Session session = hibernateUtil.getSessionFactory().openSession();

        List<Cake> list = session.getNamedQuery("Cake.findByName")
                .setParameter("name", cake.getName())
                .list();
        try {
            updateDb(session, list, cake);
        } catch (TooManyResultsException ex) {
            critical(getClass(), ex.getMessage());
            shutdown();
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public void clearDatabase() {

        log(DatabaseUtility.class, "Clearing Cakes from database");

        Session session = hibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.getNamedQuery("Cake.deleteAll").executeUpdate();
            session.getTransaction().commit();
        } catch (ConstraintViolationException ex) {
            critical(getClass(), ex.getMessage());
            throw ex;
        } finally {
            closeSession(session);
        }
    }

    public void init() throws IOException, TooManyResultsException {

        try {
            clearDatabase();
            List<Cake> cakesList = getDataLoader().init();
            mergeEntityList(cakesList);

        } catch (JDBCConnectionException ex) {
            critical(getClass(), ex.getMessage());
            shutdown();
            throw ex;
        }
    }

    public void mergeEntity(Cake cake) throws TooManyResultsException {

        Session session = hibernateUtil.getSessionFactory().openSession();
        try {
            List<Cake> list = session.getNamedQuery("Cake.findByName")
                    .setParameter("name", cake.getName())
                    .list();
            updateDb(session, list, cake);
        } finally {
            closeSession(session);
        }
    }

    public void mergeEntityList(List<? extends IEntity> entityList) throws TooManyResultsException {
        for (IEntity entity : entityList) {
            log(getClass(), "Creating entity :: " + entity.getName());
            if (entity instanceof Cake) {
                mergeEntity((Cake) entity);
            }
        }
    }

    private void updateDb(Session session, List<Cake> list, Cake cake) throws TooManyResultsException {
        Transaction transaction = session.beginTransaction();
        if (list.isEmpty()) {
            session.persist(cake);
            log(getClass(), "Adding cake :: " + cake.getName());
        } else if (list.size() > 1) {
            throw new TooManyResultsException();
        } else {
            Cake presentCake = list.get(0);
            presentCake.updateWith(cake);
            session.update(presentCake);
            log(getClass(), "Updating cake :: " + cake.getName());
        }
        transaction.commit();
    }

    public Session openSession() {
        return getHibernateUtil().getSessionFactory().openSession();
    }

    protected HibernateUtil getHibernateUtil() {
        return hibernateUtil;
    }

    public void shutdown() {
        getHibernateUtil().getSessionFactory().getCache().evictAllRegions();
        getHibernateUtil().getSessionFactory().close();
    }

    void closeSession(Session session) {
        if (session != null) {
            session.flush();
            session.disconnect();
            session.close();
        }
    }

    public List<Cake> getAll() {
        Session session;
        try {
            session = openSession();
        } catch (JDBCConnectionException ex) {
            critical(getClass(), ex);
            throw ex;
        }
        List<Cake> list = session.getNamedQuery("Cake.findAll").list();
        session.close();
        return list;
    }

    public String getAllASsJsonString() throws IOException {
        List<Cake> list = getAll();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(list);
    }

    public DataLoader getDataLoader() throws IOException {
        if (dataLoader == null) {
            dataLoader = new DataLoader();
            dataLoader.init();

        }
        return dataLoader;
    }

    public void setDataLoader(DataLoader dataLoader) {
        this.dataLoader = dataLoader;
    }

}
