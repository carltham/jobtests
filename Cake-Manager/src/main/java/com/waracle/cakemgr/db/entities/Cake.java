package com.waracle.cakemgr.db.entities;

import com.waracle.cakemgr.external.ImportedCake;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "cake")
@NamedQueries({
    @NamedQuery(name = "Cake.findAll",
            query = "SELECT c FROM Cake c")
    ,@NamedQuery(name = "Cake.deleteAll",
            query = "DELETE FROM Cake c")
    ,@NamedQuery(name = "Cake.findByName",
            query = "SELECT c FROM Cake c WHERE c.name=:name")})

public class Cake implements Serializable, IEntity {

    private static final long serialVersionUID = -1798070786993154676L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "title", unique = true, nullable = false, length = 100)
    private String name;

    @Column(name = "description", unique = false, nullable = false, length = 100)
    private String description;

    @Column(name = "image_url", unique = false, nullable = false, length = 300)
    private String imageURL;

    public Cake() {
    }

    public Cake(ImportedCake importedCake) {
        setName(importedCake.getTitle());
        setDescription(importedCake.getDesc());
        setImageURL(importedCake.getImage());
    }

    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(Object name) {
        this.name = "" + name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String image) {
        this.imageURL = image;
    }

    public void updateWith(Cake cake) {
        if (cake != null) {
            if (cake.getId() != null) {
                this.setId(cake.getId());
            }
            if (cake.getDescription() != null) {
                this.setDescription(cake.getDescription());
            }
            if (cake.getImageURL() != null) {
                this.setImageURL(cake.getImageURL());
            }
            if (cake.getName() != null) {
                this.setName(cake.getName());
            }

        }
    }

}
