/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waracle.cakemgr.utils;

/**
 *
 * @author carl
 */
public class Logger {

    public void critical(Class<?> callingClass, String message) {
        log(callingClass, message);
    }

    public void critical(Class<?> callingClass, Object object) {
        log(callingClass, object);
    }

    public void log(Class<?> callingClass, String message) {

        System.out.println("called from :" + callingClass + " - message\n" + message);
    }

    public void log(Class<?> callingClass, Object object) {
        log(callingClass, "" + object);
    }

}
