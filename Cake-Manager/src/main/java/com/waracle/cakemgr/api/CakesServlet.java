package com.waracle.cakemgr.api;

import com.waracle.cakemgr.db.utils.DatabaseUtility;
import com.waracle.cakemgr.utils.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.db.entities.Cake;
import com.waracle.cakemgr.db.exceptions.TooManyResultsException;
import java.io.IOException;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.stream.Collectors;
import javax.servlet.RequestDispatcher;
import org.hibernate.exception.JDBCConnectionException;

@WebServlet("/cakes")
public class CakesServlet extends HttpServlet {

    private static Logger servletLogger = null;

    private static DatabaseUtility databaseUtility = null;

    @Override
    public void init() throws ServletException {
        super.init();

        getLogger().log(getClass(), "init started");

        getLogger().log(getClass(), "init finished");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        try {
            redirectIfBrowser(req, resp);
        } catch (IOException | ServletException ex) {
            getLogger().critical(getClass(), ex);
        }

        String jsonInString = null;
        try {
            jsonInString = getDatabaseUtility().getAllASsJsonString();
        } catch (TooManyResultsException | IOException ex) {
            getLogger().critical(getClass(), ex);
        }
        try {
            resp.getWriter().println(jsonInString);
        } catch (IOException ex) {
            getLogger().critical(getClass(), ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        String jsonOutString = null;
        try {
            jsonOutString = addEntityFromRequest(req, resp);
        } catch (IOException | TooManyResultsException ex) {
            getLogger().critical(getClass(), ex);
        }
        try {
            resp.getWriter().println(jsonOutString);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(CakesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            getDatabaseUtility().shutdown();
        } catch (JDBCConnectionException | IOException | TooManyResultsException ex) {
            getLogger().critical(getClass(), ex);
            throw new RuntimeException(ex);
        }
    }

    private String addEntityFromRequest(HttpServletRequest req, HttpServletResponse resp) throws TooManyResultsException, IOException {
        String content = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        ObjectMapper mapper = new ObjectMapper();
        Cake cake = mapper.readValue(content, Cake.class);
        getDatabaseUtility().addEntity(cake);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.setStatus(200);
        // create HTML response
        return mapper.writeValueAsString(cake);
    }

    protected void redirectIfBrowser(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String acceptHeader = req.getHeader("Accept");
        //Accept: application/json, text/plain, */*

        if (acceptHeader.startsWith("text/html")) {
            getLogger().log(getClass(), "Browser Request Detected");
            RequestDispatcher dispatcher = getServletContext()
                    .getRequestDispatcher("/");
            dispatcher.forward(req, resp);
        } else {
            getLogger().log(getClass(), "Non Browser Request Detected");
        }
    }

    public static Logger getLogger() {
        if (servletLogger == null) {
            servletLogger = new Logger();
        }
        return servletLogger;
    }

    public static void setLogger(Logger newLogger) {
        servletLogger = newLogger;
    }

    public DatabaseUtility getDatabaseUtility() throws IOException, TooManyResultsException {
        if (databaseUtility == null) {
            databaseUtility = new DatabaseUtility();
            databaseUtility.init();

        }
        return databaseUtility;
    }

    public void setDatabaseUtility(DatabaseUtility newDatabaseUtility) {
        this.databaseUtility = newDatabaseUtility;
    }

}
