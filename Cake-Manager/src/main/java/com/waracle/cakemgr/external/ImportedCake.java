package com.waracle.cakemgr.external;

public class ImportedCake {

    private String title;

    private String desc;

    private String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String description) {
        this.desc = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String newImage) {
        this.image = newImage;
    }
}
