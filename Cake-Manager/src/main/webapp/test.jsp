<!DOCTYPE html>
<html lang='en' class=''>
    <head>

        <meta charset='UTF-8'>
        <meta name="robots" content="noindex">
        <link href="resources/css/4.0.0-alpha.6/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/default.css" rel="stylesheet" type="text/css"/>
        <script src="resources/js/axios.js" type="text/javascript"></script>
        <script src="resources/js/react-15.3.1.js" type="text/javascript"></script>
        <script src="resources/js/react-dom-15.3.1.js" type="text/javascript"></script>
        <script src="resources/js/CakeTable.js" type="text/javascript"></script>
    </script>
</head>
<body>
    <div id="container">
        <!-- This element's contents will be replaced with your component. -->
    </div>
    <!--        <script src="resources/js/index.js" type="text/javascript"></script>-->
</body>
</html>
