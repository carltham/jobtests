<%-- 
    Document   : test
    Created on : 11-Dec-2018, 17:17:38
    Author     : carl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <link href="resources/css/4.0.0-alpha.6/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/default.css" rel="stylesheet" type="text/css"/>
        
        <script src="resources/js/externals/axios.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.10.3/babel.min.js"></script>

        <script src="resources/js/adder.js" type="text/babel"></script>
        <script src="resources/js/table.js" type="text/babel"></script>
    </head>
    <body>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.1.0/react.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.1.0/react-dom.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.13.1/react-with-addons.js"></script>
        <div id="container">
            <!-- This element's contents will be replaced with your component. -->
        </div>
        <div id="table">
            <!-- This element's contents will be replaced with your component. -->
        </div>
    </body>
</html>