/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const CSSTransitionGroup = React.addons.CSSTransitionGroup;
const TransitionGroup = React.addons.TransitionGroup;
var getContacts = function () {
    axios.get('cakes')
            .then(function (response) {
// handle success
                var data = response.data;
//                var  testData = [{"employeeId":0,"name":"Lemon cheesecake","description":"A cheesecake made of lemon","imageURL":"https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg"},{"employeeId":1,"name":"victoria sponge","description":"sponge with jam","imageURL":"http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg"},{"employeeId":2,"name":"Carrot cake","description":"Bugs bunnys favourite","imageURL":"http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg"},{"employeeId":3,"name":"Banana cake","description":"Donkey kongs favourite","imageURL":"http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg"},{"employeeId":4,"name":"Birthday cake","description":"a yearly treat","imageURL":"http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg"}];
//data;
                React.render(<Table cakesList={data} filterText=''/>, document.getElementById('table'));
                return data;
            })
            .catch(function (error) {
// handle error
                console.log(error);
            })
            .then(function () {
// always executed
                console.log("In getContacts .then()");
            });
}();

class Table extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        var _this3 = this;
        var rows = [];
        this.props.cakesList.forEach(function (cake) {
            if (cake.name.indexOf(_this3.props.filterText) === -1) {
                return;
            }
            rows.push(React.createElement(Row, {cake: cake}));
        });

        return (
                React.createElement(
                        "table",
                        {className: "table"},
                        React.createElement(
                                "thead", null,
                                React.createElement(
                                        "tr",
                                        null,
                                        React.createElement("th", null, "Title"),
                                        React.createElement("th", null, "Description"),
                                        React.createElement("th", null, "Image"))),
                        React.createElement("tbody", null, rows)));
    }
}

class Row extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const image = React.createElement('img', {
            src: this.props.cake.imageURL,
            style: {width: '100px'}
        });
        return (React.createElement("tr", null,
                React.createElement("td", null, this.props.cake.name),
                React.createElement("td", null, this.props.cake.description),
                React.createElement("td", null, image)));
    }
}