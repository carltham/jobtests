/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const CSSTransitionGroup = React.addons.CSSTransitionGroup;
const TransitionGroup = React.addons.TransitionGroup;

class SlidingPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {visible: false};
        this.handleAddClick = this.handleAddClick.bind(this)
    }

    handleAddClick() {
        this.setState({visible: !this.state.visible});
    }

    render() {

        return <div> 
            <button onClick={this.handleAddClick}>{this.state.visible ? 'Hide' : 'Add' }</button>
        
        
            <CSSTransitionGroup transitionName="example">
                { this.state.visible ? <div className='panel' ><CakeAdder class="panel" /></div> : null }
            </CSSTransitionGroup>
        </div>
    }
}

class CakeAdder extends React.Component {

    constructor(props) {
        super(props);
        if (props.newCake) {
            this.newCake = props.newCake
        } else {
            this.newCake = {
                'name': '',
                'description': '',
                'imageURL': ''
            }
        }

        this.state = {visible: false, newCake: this.newCake};
        this.handleAddClick = this.handleAddClick.bind(this)
        this.handleAddTextInputChange = this.handleAddTextInputChange.bind(this)
    }

    handleAddTextInputChange(source, event) {
        console.log('event = ' + event);
        source.newCake[event.target.id] = event.target.value
        this.setState({visible: !this.state.visible});
    }

    handleAddClick(source, event) {
        console.log('handleAddClick - The add button was clicked.');
        console.log('source.newCake = ' + source.newCake);
        this.setState({visible: !this.state.visible});
        axios.post('cakes', source.newCake)
                .then(function (response) {
                    console.log("In handleAddClickaxios.post.response()" + response);
                })
                .catch(function (error) {
                    console.log("In handleAddClickaxios.post.error()" + error);
                })
                .then(function () {
// always executed
                    console.log("In handleAddClickaxios.axios.post.finally()");
                });
    }

    render() {
        return (
                React.createElement("form", null,
                        React.createElement(
                                "input",
                                {
                                    id: "name",
                                    className: "form-control left twentyfive_percent",
                                    type: "text",
                                    placeholder: "Title...",
                                    value: this.newCake.name,
                                    onChange: (e) => this.handleAddTextInputChange(this, e)
                                }),
                        React.createElement(
                                "input",
                                {
                                    id: "description",
                                    className: "form-control left twentyfive_percent",
                                    type: "text",
                                    placeholder: "Description...",
                                    value: this.newCake.description,
                                    onChange: (e) => this.handleAddTextInputChange(this, e)
                                }),
                        React.createElement(
                                "input",
                                {
                                    id: "imageURL",
                                    className: "form-control left twentyfive_percent",
                                    type: "text",
                                    placeholder: "Image url ...",
                                    value: this.newCake.imageURL,
                                    onChange: (e) => this.handleAddTextInputChange(this, e)
                                }),
                        React.createElement(
                                "div",
                                {
                                    className: "center left twentyfive_percent"
                                },
                                React.createElement(
                                        "button",
                                        {
                                            className: "button lefttwentyfive_percent",
                                            type: "cancel",
                                        },
                                        "Cancel"),
                                React.createElement(
                                        "button",
                                        {
                                            onClick: (e) => this.handleAddClick(this, e),
                                            className: "button lefttwentyfive_percent",
                                            type: "submit"
                                        },
                                        "Add")
                                )
                        )

                );
    }
}

//React.render(<CakeAdder />, document.getElementById('table'));
React.render(<SlidingPanel />, document.getElementById('container'));