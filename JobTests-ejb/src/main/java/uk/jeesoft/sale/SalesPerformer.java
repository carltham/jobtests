/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.sale;

import uk.jeesoft.db.builders.SalesPostBuilder;
import uk.jeesoft.db.handlers.ProductHandler;
import java.util.ArrayList;
import java.util.List;
import uk.jeesoft.db.Product;
import uk.jeesoft.db.Sale;
import uk.jeesoft.db.SalesPost;
import uk.jeesoft.db.handlers.OfferHandler;

/**
 *
 * @author carl
 */
public class SalesPerformer {

    private final ProductHandler productHandler;
    private final OfferHandler offerHandler;

    public SalesPerformer() throws IllegalAccessException, InstantiationException {
        productHandler = new ProductHandler();
        offerHandler = new OfferHandler();
    }

    public Sale perform(List<Product> productList) {
        Sale sale = new Sale();
        for (Product product : productList) {
            SalesPost item = new SalesPostBuilder().withProduct(product).build();
            sale.addSalesPost(item);
        }
        sale.applyOffers(offerHandler.getAll());
        return sale;
    }

    public Sale perform(String[] productArray) {
        List<Product> productList = new ArrayList<>();
        for (String productName : productArray) {
            Product product = productHandler.findByName(productName);
            productList.add(product);
        }
        return perform(productList);
    }

    public ProductHandler getProductHandler() {
        return productHandler;
    }
}
