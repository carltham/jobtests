/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.config;

import java.util.Locale;

/**
 *
 * @author carl
 */
public enum CurrencyType {
    GBP("£", "p", Locale.ENGLISH);
    private String mainSymbol;
    private String secondarySymbol;

    private CurrencyType(String newMainSymbol, String newSecondarySymbol, Locale newLocale) {
        mainSymbol = newMainSymbol;
        secondarySymbol = newSecondarySymbol;
    }

    public String getMainSymbol() {
        return mainSymbol;
    }

    public String getSecondarySymbol() {
        return secondarySymbol;
    }
}
