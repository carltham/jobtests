/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.jeesoft.db.interfaces.INamedInstance;

/**
 *
 * @author carl
 */
public class MapUtil {

    public static <T> Map<String, T> clone(Map<String, T> sourceMap) {
        Map<String, T> targetMap = new HashMap<>();
        for (Map.Entry<String, T> entry : sourceMap.entrySet()) {
            targetMap.put(entry.getKey(), entry.getValue());
        }

        return targetMap;
    }

    public static <T extends INamedInstance> Map<String, T> createMap(List<T> sourceList) {
        Map<String, T> targetMap = new HashMap<>();
        for (T instance : sourceList) {
            targetMap.put(instance.getName(), instance);
        }
        return targetMap;
    }

}
