/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.jeesoft.db.Product;

/**
 *
 * @author carl
 */
public class PojoUtils {

    public static boolean compareFields(Object source, Object target, String commaSeparatedFieldsListString)
            throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        String[] fieldsListArray = commaSeparatedFieldsListString.split(",");
        boolean isEquals = true;
        for (String fieldName : fieldsListArray) {
            fieldName = fieldName.trim();
            String getterName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            Method sourceGetter = null;
            try {
                sourceGetter = source.getClass().getMethod(getterName);
            } catch (NoSuchMethodException e) {
                // Might be a oolean field
                getterName = "is" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                sourceGetter = source.getClass().getMethod(getterName);
            }
            Object sourceValue = sourceGetter.invoke(source);
            Method targetGetter = target.getClass().getMethod(getterName);
            Object targetValue = targetGetter.invoke(target);
            isEquals &= sourceValue.equals(targetValue);
        }
        return isEquals;
    }

    public static int calculateHasch(Object pojo, String commaSeparatedFieldsListString) throws NoSuchFieldException {

        int hash = 0;
        String[] fieldsListArray = commaSeparatedFieldsListString.split(",");
        for (String fieldName : fieldsListArray) {
            try {
                Field field = pojo.getClass().getDeclaredField(fieldName);
                hash += (field != null ? field.hashCode() : 0);
            } catch (NoSuchFieldException ex) {
                Logger.getLogger(Product.class.getName()).log(Level.SEVERE, ex.getClass().getSimpleName() + " : " + ex.getMessage());
            }
        }
        return hash;
    }

}
