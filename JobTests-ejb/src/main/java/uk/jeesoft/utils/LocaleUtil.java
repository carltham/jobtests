/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.utils;

import java.text.DecimalFormat;

/**
 *
 * @author carl
 */
public class LocaleUtil {

    public static double preformatDecimals(double value, int nrOfDecimals) throws NumberFormatException {
        String strValue = preformatDecimalsAsString(value, nrOfDecimals);
        value = Double.parseDouble(strValue);
        return value;
    }

    public static String preformatDecimalsAsString(double value, int nrOfDecimals) {
        String strDecimals = "";
        for (int decimalNr = 0; decimalNr < nrOfDecimals; decimalNr++) {
            strDecimals += "0";
        }
        DecimalFormat decimalFormat = new DecimalFormat("#." + strDecimals);
        String strValue = decimalFormat.format(value);
        return strValue;
    }
}
