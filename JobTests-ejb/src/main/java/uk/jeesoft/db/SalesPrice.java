/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import uk.jeesoft.config.CurrencyType;
import uk.jeesoft.domain.HashedObject;

/**
 *
 * @author carl
 */
@Entity
public class SalesPrice extends HashedObject {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private final CurrencyType currencyType;

    private double value;

    public SalesPrice() {
        currencyType = CurrencyType.GBP;
    }

    public SalesPrice(double newValue) {
        this();
        value = newValue;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    protected String getEqualsComparisionFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
