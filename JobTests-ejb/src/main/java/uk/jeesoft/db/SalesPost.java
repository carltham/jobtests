/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db;

import java.text.DecimalFormat;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import uk.jeesoft.domain.HashedObject;
import uk.jeesoft.utils.LocaleUtil;

/**
 *
 * @author carl
 */
@Entity
public class SalesPost extends HashedObject {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Product product;
    private int amount;
    private double _rebate;
    private String rebateString;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getRebate() {
        return LocaleUtil.preformatDecimals(_rebate, 1);
    }
//

    public void setRebate(Double newRebate, String formattedValue) {
        _rebate = newRebate;
        setRebateString(formattedValue);
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    String getRebateString() {
        return rebateString;
    }

    private void setRebateString(String formattedValue) {
        String formattedRebate = "";
        double rebate = getRebate();
        if (rebate < 0) {
            DecimalFormat decimalFormatLt1 = new DecimalFormat("#");
            formattedRebate = decimalFormatLt1.format(rebate * 100) + product.getPrice().getCurrencyType().getSecondarySymbol();
            rebateString = product.getName() + " " + formattedValue
                    + " off: " + formattedRebate + "\n";
        }
    }

    void increaseAmount() {
        amount++;
    }

    @Override
    protected String getEqualsComparisionFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
