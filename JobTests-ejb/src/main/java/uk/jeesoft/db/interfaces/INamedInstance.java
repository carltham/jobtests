/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.interfaces;

import java.io.Serializable;

/**
 *
 * @author carl
 */
public interface INamedInstance extends Serializable {

    public String getName();

}
