/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.builders;

import uk.jeesoft.db.Product;
import uk.jeesoft.db.SalesPrice;

/**
 *
 * @author carl
 */
public class ProductBuilder extends AbstractBuilder<Product> {

    public ProductBuilder() throws InstantiationException, IllegalAccessException {
        super(Product.class);
    }

    public ProductBuilder withName(String apples) {
        getInstance().setName(apples);
        return this;
    }

    public ProductBuilder withPrice(SalesPrice newPrice) {
        getInstance().setPrice(newPrice);
        return this;
    }
}
