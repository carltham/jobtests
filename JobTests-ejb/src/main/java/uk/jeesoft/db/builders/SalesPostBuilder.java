/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.builders;

import uk.jeesoft.db.Product;
import uk.jeesoft.db.SalesPost;

/**
 *
 * @author carl
 */
public class SalesPostBuilder {

    private final SalesPost salesPost;

    public SalesPostBuilder() {
        salesPost = new SalesPost();
    }

    public SalesPostBuilder withProduct(Product product) {
        salesPost.setProduct(product);
        salesPost.setAmount(1);
        return this;
    }

    public SalesPost build() {
        return salesPost;
    }
}
