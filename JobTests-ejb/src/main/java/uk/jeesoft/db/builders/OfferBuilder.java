/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.builders;

import uk.jeesoft.db.Offer;
import uk.jeesoft.db.types.OfferType;

/**
 *
 * @author carl
 */
public class OfferBuilder extends AbstractBuilder<Offer> {

    public OfferBuilder() throws InstantiationException, IllegalAccessException {
        super(Offer.class);
    }

    public OfferBuilder withOfferType(OfferType offerType) {
        getInstance().setOfferType(offerType);
        return this;
    }

    public OfferBuilder withValue(double newValue) {
        getInstance().setOfferValue(newValue);
        return this;
    }

    public OfferBuilder withProductName(String productName) {
        getInstance().setProductName(productName);
        return this;
    }

    public OfferBuilder withReferenceProductName(String referenceProductName) {
        getInstance().setReferenceProductName(referenceProductName);
        return this;
    }

    public OfferBuilder withReferenceProductAmount(int amount) {
        getInstance().setReferenceProductAmount(amount);
        return this;
    }
}
