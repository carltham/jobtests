/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.builders;

/**
 *
 * @author carl
 */
public abstract class AbstractBuilder<T> {

    private final T instance;

    public AbstractBuilder(Class<T> clazz) throws InstantiationException, IllegalAccessException {
        instance = clazz.newInstance();
    }

    public T getInstance() {
        return instance;
    }

    public T build() {
        return instance;
    }
}
