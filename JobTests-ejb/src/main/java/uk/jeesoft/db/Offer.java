/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db;

import uk.jeesoft.db.types.OfferType;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import uk.jeesoft.db.interfaces.INamedInstance;
import static uk.jeesoft.db.types.OfferType.NONE;
import static uk.jeesoft.db.types.OfferType.PERCENT;
import uk.jeesoft.domain.HashedObject;

/**
 *
 * @author carl
 */
@Entity
@NamedQueries(value = {
    @NamedQuery(name = "Offer.getAll", query = "SELECT o FROM Offer o")
    ,@NamedQuery(name = "Offer.getByName", query = "SELECT o FROM Offer o WHERE o.productName = :name")
})
public class Offer extends HashedObject implements INamedInstance {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private OfferType offerType;
    private double offerValue;
    private String productName;
    private String referenceProductName;
    private int amountOfExpectedReferenceProduct;

    public Offer() {
        offerType = NONE;
    }

    void process(Map<String, SalesPost> salesPostMap) {

        SalesPost offerSalesPost = salesPostMap.get(getProductName());
        int nrOfProductsOnOffer = offerSalesPost.getAmount();
        String salesProductName = offerSalesPost.getProduct().getName();
        if (salesProductName != getProductName()) {
            throw new IllegalArgumentException("This Offer (" + offerType.name() + ") cannot be applied on product :" + salesProductName);
        }
        Product product = offerSalesPost.getProduct();
        double rebate = 0;
        String formattedText = "";
        switch (offerType) {
            case PERCENT: {
                rebate = offerValue / 100 * product.getPrice().getValue() * nrOfProductsOnOffer;
            }
            formattedText = offerValue + offerType.getSymbol() + " x " + nrOfProductsOnOffer;
            break;
            case HALF_OF_ONE_FOR_EACH_TWO:
                SalesPost referenceSalesPost = salesPostMap.get(getReferenceProductName());
                int amountOfAffectedProducts = offerSalesPost.getAmount();
                if (referenceSalesPost != null) {
                    int nrOfRebatesApplied = 0;
                    for (int productOnOfferNr = 0; productOnOfferNr < nrOfProductsOnOffer; productOnOfferNr++) {
                        int amountOfAffectedReferenceProducts = referenceSalesPost.getAmount() - nrOfRebatesApplied * amountOfExpectedReferenceProduct;
                        int nrOfRebateApplications = (amountOfAffectedProducts > 0)
                                && (amountOfAffectedReferenceProducts - amountOfAffectedProducts >= 0)
                                        ? 1 : 0;
                        if (nrOfRebateApplications > 0) {
                            nrOfRebatesApplied++;
                            rebate += offerSalesPost.getProduct().getPrice().getValue() / 2 * nrOfRebateApplications;
                        }

                    }
                    formattedText = ":: " + amountOfExpectedReferenceProduct + " "
                            + referenceProductName + " - get a " + productName + " for half price x " + nrOfRebatesApplied;
                }
                break;
        }
        offerSalesPost.setRebate(-1 * rebate, formattedText);

    }

    public void setId(Long id) {
        this.id = id;
    }

    public OfferType getOfferType() {
        return offerType;
    }

    public void setOfferType(OfferType newOfferType) {
        offerType = newOfferType;
    }

    public double getOfferValue() {
        return offerValue;
    }

    public void setOfferValue(double newValue) {
        offerValue = newValue;
    }

    public String getName() {
        return productName;
    }

    private String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    private String getReferenceProductName() {
        return referenceProductName;
    }

    public void setReferenceProductName(String referenceProductName) {
        this.referenceProductName = referenceProductName;
    }

    public int getReferenceProductAmount() {
        return amountOfExpectedReferenceProduct;
    }

    public void setReferenceProductAmount(int amount) {
        amountOfExpectedReferenceProduct = amount;
    }

    @Override
    protected String getEqualsComparisionFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
