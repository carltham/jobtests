/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.types;

/**
 *
 * @author carl
 */
public enum OfferType {
    NONE(""), PERCENT("%"), HALF_OF_ONE_FOR_EACH_TWO("");
    private String symbol;

    private OfferType(String newSymbol) {
        symbol = newSymbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
