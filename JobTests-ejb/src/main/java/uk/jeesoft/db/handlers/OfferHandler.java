/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.handlers;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.TypedQuery;
import uk.jeesoft.db.Offer;
import uk.jeesoft.db.builders.OfferBuilder;
import uk.jeesoft.db.types.OfferType;

/**
 *
 * @author carl
 */
public class OfferHandler extends AbstractDBHandler<Offer> {

    public OfferHandler() throws InstantiationException, IllegalAccessException {
        init();
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        OfferHandler offerHandler = new OfferHandler();
        offerHandler.clearDB();
    }

    public void clearDB() {
        TypedQuery<Offer> typedQuery = getEntityManager().createNamedQuery("Offer.getAll", Offer.class);
        List<Offer> offers = typedQuery.getResultList();
        removeList(offers);
        close();
    }

    List<Offer> init() throws InstantiationException, IllegalAccessException {
        List<Offer> offerList = getAll();
        if (offerList.isEmpty()) {
            offerList = new ArrayList<>();
            offerList.add(new OfferBuilder().withOfferType(OfferType.PERCENT).withProductName("Apples")
                    .withValue(10d).build());
            offerList.add(new OfferBuilder().withOfferType(OfferType.HALF_OF_ONE_FOR_EACH_TWO)
                    .withProductName("Bread").withReferenceProductName("Soup")
                    .withReferenceProductAmount(2).build());
            persistList(offerList);
        }
        return offerList;
    }

    public List<Offer> getAll() {
        TypedQuery<Offer> typedQuery = getEntityManager().createNamedQuery("Offer.getAll", Offer.class);
        List<Offer> offers = typedQuery.getResultList();
        return offers;
    }
}
