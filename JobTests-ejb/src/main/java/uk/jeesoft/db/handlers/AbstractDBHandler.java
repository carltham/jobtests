/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.handlers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author carl
 */
public abstract class AbstractDBHandler<T> {

    protected static EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;

    public AbstractDBHandler() {
        try {
            if (entityManagerFactory == null) {
                entityManagerFactory = Persistence.createEntityManagerFactory("uk.jeesoft_JobTests-ejb_ejb_1.0-SNAPSHOTPU");
            }
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    private void persist(T entity) {
        getEntityManager().persist(entity);
    }

    public void persistList(List<T> entityList) {
        getEntityManager().getTransaction().begin();
        for (T entity : entityList) {
            persist(entity);
        }
        getEntityManager().getTransaction().commit();
    }

    private void remove(T entity) {
        getEntityManager().remove(entity);
    }

    protected void removeList(List<T> entityList) {
        getEntityManager().getTransaction().begin();
        for (T entity : entityList) {
            remove(entity);
        }
        getEntityManager().getTransaction().commit();
    }

    protected void close() {
        getEntityManager().clear();
        getEntityManager().close();
    }

    protected EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = entityManagerFactory.createEntityManager();
        }
        return entityManager;
    }

    boolean isOpen() {
        return getEntityManager().isOpen();
    }
}
