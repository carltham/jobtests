/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.handlers;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.TypedQuery;
import uk.jeesoft.db.Product;
import uk.jeesoft.db.SalesPrice;
import uk.jeesoft.db.builders.ProductBuilder;

/**
 *
 * @author carl
 */
public class ProductHandler extends AbstractDBHandler<Product> {

    public ProductHandler() throws IllegalAccessException, InstantiationException {
        init();
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        ProductHandler productHandler = new ProductHandler();
        productHandler.clearDB();
    }

    public void clearDB() {
        TypedQuery<Product> typedQuery = getEntityManager().createNamedQuery("Product.getAll", Product.class);
        List<Product> products = typedQuery.getResultList();
        removeList(products);
        close();
    }

    List<Product> init() throws IllegalAccessException, InstantiationException {
        List<Product> productList = getAll();
        if (productList.isEmpty()) {
            productList = new ArrayList<>();
            productList.add(new ProductBuilder().withName("Soup").withPrice(new SalesPrice(.65)).build());
            productList.add(new ProductBuilder().withName("Bread").withPrice(new SalesPrice(.8)).build());
            productList.add(new ProductBuilder().withName("Milk").withPrice(new SalesPrice(1.3)).build());
            productList.add(new ProductBuilder().withName("Apples").withPrice(new SalesPrice(1d)).build());
            productList.add(new ProductBuilder().withName("Butter").withPrice(new SalesPrice(.9)).build());
            productList.add(new ProductBuilder().withName("Salt").withPrice(new SalesPrice(.4)).build());
            persistList(productList);
        }
        return productList;
    }

    public List<Product> getAll() {
        TypedQuery<Product> typedQuery = getEntityManager().createNamedQuery("Product.getAll", Product.class);
        List<Product> products = typedQuery.getResultList();
        return products;
    }

    public Product findByName(String productName) {
        TypedQuery<Product> typedQuery = getEntityManager().createNamedQuery("Product.getByName", Product.class);
        typedQuery.setParameter("name", productName);
        Product product = typedQuery.getSingleResult();
        return product;
    }

    public Product findById(Long id) {
        return getEntityManager().find(Product.class, id);
    }

    public String listAll() {
        List<Product> productList = getAll();
        String productListString = "These products are available :\n";
        for (Product product : productList) {
            productListString += product.getName() + "\n";
        }
        return productListString + "\n";
    }

}
