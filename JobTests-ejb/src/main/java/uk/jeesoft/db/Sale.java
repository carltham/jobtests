/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import uk.jeesoft.config.CurrencyType;
import uk.jeesoft.db.exception.ProductsOfDifferentCurrenciesMixedException;
import uk.jeesoft.domain.HashedObject;
import uk.jeesoft.utils.LocaleUtil;
import uk.jeesoft.utils.MapUtil;

/**
 *
 * @author carl
 */
@Entity
public class Sale extends HashedObject {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany
    private Map<String, SalesPost> salesPostMap;

    public Sale() {
        salesPostMap = new HashMap<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getSubtotal() {
        double subtotal = 0;
        for (Map.Entry<String, SalesPost> entry : salesPostMap.entrySet()) {
            SalesPost salesPost = entry.getValue();
            subtotal += salesPost.getAmount() * salesPost.getProduct().getPrice().getValue();
        }
        return LocaleUtil.preformatDecimals(subtotal, 1);
    }

    public Double getRebate() {
        double rebate = 0;
        for (Map.Entry<String, SalesPost> entry : salesPostMap.entrySet()) {
            SalesPost salesPost = entry.getValue();
            rebate += salesPost.getRebate();
        }
        rebate = LocaleUtil.preformatDecimals(rebate, 1);
        return rebate;
    }

    public double getTotal() {
        return LocaleUtil.preformatDecimals(getSubtotal() + getRebate(), 1);
    }

    @Override
    public String toString() {

        DecimalFormat decimalFormatGt1 = new DecimalFormat("#.00");
        String rebateString = "";
        CurrencyType currencyType = null;
        for (Map.Entry<String, SalesPost> entry : salesPostMap.entrySet()) {
            SalesPost salesPost = entry.getValue();
            Product product = salesPost.getProduct();
            if (currencyType != null && product.getPrice().getCurrencyType() != currencyType) {
                throw new ProductsOfDifferentCurrenciesMixedException();
            }
            currencyType = product.getPrice().getCurrencyType();
            double rebate = Math.abs(salesPost.getRebate());
            if (rebate > 0) {
                rebateString += salesPost.getRebateString();
            }

        }
        if (rebateString.isEmpty()) {
            rebateString = "(no offers available)\n";
        }
        String formattedSubtotal = currencyType.getMainSymbol() + decimalFormatGt1.format(getSubtotal());
        String formattedTotal = currencyType.getMainSymbol() + decimalFormatGt1.format(getTotal());
        return "\nSubtotal: " + formattedSubtotal + "\n"
                + rebateString
                + "Total: " + formattedTotal + "\n";
    }

    public void addSalesPost(SalesPost newSalesPost) {
        SalesPost existingSalesPost = salesPostMap.get(newSalesPost.getProduct().getName());
        if (existingSalesPost != null) {
            existingSalesPost.increaseAmount();
        } else {
            salesPostMap.put(newSalesPost.getProduct().getName(), newSalesPost);
        }
    }

    public void applyOffers(List<Offer> offersList) {
        Map<String, SalesPost> tmpSalesPostMap = MapUtil.clone(salesPostMap);
        Map<String, Offer> tmpOffersMap = MapUtil.createMap(offersList);
        Map<String, SalesPost> offerSalesPostMap = new HashMap<>();
        for (Map.Entry<String, Offer> entry : tmpOffersMap.entrySet()) {
            String productName = entry.getKey();
            if (tmpSalesPostMap.containsKey(productName)) {
                SalesPost salesPost = tmpSalesPostMap.remove(productName);
                offerSalesPostMap.put(salesPost.getProduct().getName(), salesPost);
            }
        }
        for (Map.Entry<String, SalesPost> entry : offerSalesPostMap.entrySet()) {
            String productName = entry.getValue().getProduct().getName();
            Offer offer = tmpOffersMap.get(productName);
            offer.process(salesPostMap);
        }
    }

    @Override
    protected String getEqualsComparisionFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
