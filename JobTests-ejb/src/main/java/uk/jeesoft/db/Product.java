/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import uk.jeesoft.db.interfaces.INamedInstance;
import uk.jeesoft.domain.HashedObject;

/**
 *
 * @author carl
 */
@Entity
@NamedQueries(value = {
    @NamedQuery(name = "Product.getAll", query = "SELECT p FROM Product p")
    ,
    @NamedQuery(name = "Product.getByName", query = "SELECT p FROM Product p WHERE p.name = :name")
})
public class Product extends HashedObject implements INamedInstance {

//    @Transient
//    private String EQUAL_COMPARISION_FIELDS = "id, name,description,price";
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String description;

    @OneToOne(cascade = CascadeType.ALL)
    private SalesPrice price;

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SalesPrice getPrice() {
        return price;
    }

    public void setPrice(SalesPrice newPrice) {
        price = newPrice;
    }

    @Override
    public String toString() {
        return "uk.jeesoft.NewEntity[ id=" + id + " ]";
    }

    @Override
    protected String getEqualsComparisionFields() {
//        return EQUAL_COMPARISION_FIELDS;
        return "id, name,description,price";
    }
}
