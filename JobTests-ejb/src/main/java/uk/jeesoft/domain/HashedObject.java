/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.domain;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.jeesoft.db.Product;
import uk.jeesoft.utils.PojoUtils;

/**
 *
 * @author carl
 */
public abstract class HashedObject implements Serializable {

    @Override
    public int hashCode() {
        int hash = 0;
        try {
            hash = PojoUtils.calculateHasch(this, getEqualsComparisionFields());
        } catch (NoSuchFieldException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, ex.getClass().getSimpleName() + " : " + ex.getMessage());
        }
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        boolean fieldsAreEquals = false;
        try {
            Object other = this.getClass().cast(object);
            if (other == null) {
                return false;
            }
            fieldsAreEquals = PojoUtils.compareFields(this, other, getEqualsComparisionFields());
        } catch (ClassCastException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, ex.getClass().getSimpleName() + " : " + ex.getMessage());
        }
        return fieldsAreEquals;
    }

    protected abstract String getEqualsComparisionFields();

}
