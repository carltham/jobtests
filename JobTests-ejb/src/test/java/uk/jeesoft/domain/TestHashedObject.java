/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.domain;

/**
 *
 * @author carl
 */
public class TestHashedObject extends HashedObject {
    
    private String testField3;
    private Boolean testField4;

    TestHashedObject(String stringValue, boolean booleanValue) {
        testField3 = stringValue;
        testField4 = booleanValue;
    }

    TestHashedObject() {
        testField3 = "test";
        testField4 = false;
    }

    public String getEqualsComparisionFields() {
        return "testField3, testField4";
    }

    public String getTestField3() {
        return testField3;
    }

    public void setTestField3(String testField3) {
        this.testField3 = testField3;
    }

    public boolean isTestField4() {
        return testField4;
    }

    public void setTestField4(boolean testField4) {
        this.testField4 = testField4;
    }
    
}
