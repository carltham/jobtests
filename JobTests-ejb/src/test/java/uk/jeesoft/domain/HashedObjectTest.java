/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.domain;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author carl
 */
public class HashedObjectTest {

    private TestHashedObject instance;

    @Before
    public void setUp() {
        instance = new TestHashedObject();
    }

    /**
     * Test of hashCode method, of class HashedObject.
     */
    @Test
    public void testHashCode() {

        int expResult = -13545891;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class HashedObject.
     */
    @Test
    public void testEquals_Is_False_By_Null() {
        TestObject object = null;
        boolean result = instance.equals(object);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void testEquals_Is_False_By_Other_Object() {
        TestObject object = new TestObject();
        boolean result = instance.equals(object);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void testEquals_Is_False_By_Value() {
        TestHashedObject object = new TestHashedObject("testtttt", false);
        boolean result = instance.equals(object);
        Assertions.assertThat(result).isFalse();
    }

    @Test
    public void testEquals_Is_True() {
        TestHashedObject object = new TestHashedObject("test", false);
        boolean result = instance.equals(object);
        Assertions.assertThat(result).isTrue();
    }

    /**
     * Test of getEqualsComparisionFields method, of class HashedObject.
     */
    @Test
    public void testGetEqualsComparisionFields() {
        String expResult = "testField3, testField4";
        String result = instance.getEqualsComparisionFields();
        assertEquals(expResult, result);
    }

    private static class TestObject {

        String testField1;
        boolean testField2;

        public TestObject() {
        }
    }


}
