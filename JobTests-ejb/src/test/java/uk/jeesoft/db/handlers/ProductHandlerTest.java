/* * To change this license header, choose License Headers in Project Properties. * To change this template file, choose Tools | Templates * and open the template in the editor. */package uk.jeesoft.db.handlers;

import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.jeesoft.db.Product;
import uk.jeesoft.db.SalesPrice;
import uk.jeesoft.db.builders.ProductBuilder;

/**
 * * * @author carl
 */
public class ProductHandlerTest {

    ProductHandler productHandler;

    @Before
    public void setup() throws IllegalAccessException, InstantiationException {
        productHandler = new ProductHandler();
    }

    @After
    public void teardown() {
        if (productHandler.isOpen()) {
            productHandler.clearDB();
        }
    }

    @Test
    public void testGetAll() {

        List<Product> result = productHandler.getAll();
        assertThat(result).isNotEmpty();
    }

    @Test
    public void testFindByName() throws InstantiationException, IllegalAccessException {
        String productName = "Bread";

        Product expResult = new ProductBuilder().withName("Bread").withPrice(new SalesPrice(1d)).build();
        Product result = productHandler.findByName(productName);
        assertThat(expResult.getName()).isEqualTo(result.getName());
    }

    @Test
    public void testPersistList() throws InstantiationException, IllegalAccessException {
        List<Product> testList = new ArrayList<Product>();
        testList.add(new ProductBuilder().withName("tttt1").withPrice(new SalesPrice(1.1)).build());
        testList.add(new ProductBuilder().withName("tttt2").withPrice(new SalesPrice(1d)).build());
        testList.add(new ProductBuilder().withName("tttt3").withPrice(new SalesPrice(1d)).build());
        productHandler.persistList(testList);

        for (Product product : testList) {
            Product testedProduct = productHandler.findByName(product.getName());
            assertThat(testedProduct).isNotNull();
        }
    }

    /**
     * Test of main method, of class ProductHandler.
     */
    @Test
    public void testMain() throws Exception {
        String[] args = null;
        ProductHandler.main(args);
    }

    /**
     * Test of clearDB method, of class ProductHandler.
     */
    @Test
    public void testClearDB() {

        productHandler.clearDB();
    }

    /**
     * Test of init method, of class ProductHandler.
     */
    @Test
    public void testInit() throws Exception {

        List<Product> result = productHandler.init();
        assertThat(result.size()).isEqualTo(6);
    }

    /**
     * Test of findById method, of class ProductHandler.
     */
    @Test
    public void testFindById() {

        List<Product> productList = productHandler.getAll();
        Product product = productList.get(0);

        Product result = productHandler.findById(product.getId());
        assertThat(result.getName()).isEqualTo(product.getName());
    }

    /**
     * Test of listAll method, of class ProductHandler.
     */
    @Test
    public void testListAll() {
        String expResult = "These products are available :";
        String result = productHandler.listAll();
        assertThat(result.startsWith(expResult)).isTrue();
    }
}
