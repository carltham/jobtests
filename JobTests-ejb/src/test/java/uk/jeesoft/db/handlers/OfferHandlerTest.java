/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.db.handlers;

import java.util.List;
import javax.persistence.EntityManager;
import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import uk.jeesoft.db.Offer;

/**
 *
 * @author carl
 */
public class OfferHandlerTest {

    OfferHandler instance;

    @Before
    public void setup() throws IllegalAccessException, InstantiationException {
        instance = new OfferHandler();
    }

    @After
    public void teardown() {
        if (instance.isOpen()) {
            instance.clearDB();
        }
    }

    @Test
    public void testMain() throws Exception {
        System.out.println("main");
        String[] args = null;
        OfferHandler.main(args);
    }

    @Test
    public void testClearDB() throws InstantiationException, IllegalAccessException {

        List<Offer> offerList = instance.getAll();
        assertThat(offerList.size()).isEqualTo(2);
        instance.clearDB();
        EntityManager entityManager = instance.getEntityManager();
        assertThat(entityManager.isOpen()).isFalse();
    }

    @Test
    public void testInit() throws Exception {

        List<Offer> result = instance.init();
        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    public void testGetAll() throws InstantiationException, IllegalAccessException {

        List<Offer> result = instance.getAll();
        Assertions.assertThat(result.size()).isEqualTo(2);
    }
}
