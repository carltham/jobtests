/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.sales;

import java.util.Map;
import javax.persistence.NoResultException;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import uk.jeesoft.sale.SalesPerformer;
import org.junit.Before;
import org.junit.Test;
import uk.jeesoft.db.Product;
import uk.jeesoft.db.Sale;
import uk.jeesoft.utils.LocaleUtil;
import uk.jeesoft.utils.MapUtil;

/**
 *
 * @author carl
 */
public class SalePerformerPreLoadedTest {

    private SalesPerformer salesPerformer;

    @Before
    public void setUp() throws IllegalAccessException, InstantiationException {
        salesPerformer = new SalesPerformer();
    }

    @Test
    public void nonFailingTestWithOffer_apples() {
//Apples have 10% off their normal price this week

        String[] args = new String[]{"Apples", "Milk", "Bread"};
        Sale result = salesPerformer.perform(args);

        Map<String, Product> productMap = MapUtil.createMap(salesPerformer.getProductHandler().getAll());
        Product apples = productMap.get("Apples");
        Product milk = productMap.get("Milk");
        Product bread = productMap.get("Bread");
        double expectedSubtotal = LocaleUtil.preformatDecimals(apples.getPrice().getValue() + milk.getPrice().getValue()
                + bread.getPrice().getValue(), 2);

        String expectedSubtotalAsString = LocaleUtil.preformatDecimalsAsString(expectedSubtotal, 2);
        double expectedTotalRebate = LocaleUtil.preformatDecimals(apples.getPrice().getValue() * .1 * -1, 1);
        double expectedTotal = LocaleUtil.preformatDecimals(expectedSubtotal + expectedTotalRebate, 1);
        String expectedTotalAsString = LocaleUtil.preformatDecimalsAsString(expectedTotal, 2);

        String expectedOutput = "\nSubtotal: £" + expectedSubtotalAsString + "\n"
                + "Apples 10.0% x 1 off: " + (int) (expectedTotalRebate * 100) + "p\n"
                + "Total: £" + expectedTotalAsString + "\n";

        Double subtotal = result.getSubtotal();
        Double rebate = result.getRebate();
        double total = result.getTotal();

        Object[][] objectArray = new Object[][]{
            {subtotal, expectedSubtotal},
            {rebate, expectedTotalRebate},
            {total, expectedTotal},
            {result.toString(), expectedOutput}};
        verify(objectArray);
    }

    @Test
    public void testNonFailingWithTwoBreadOfferOneApple() {

// Actual
        String[] args = new String[]{"Apples", "Bread", "Soup", "Soup", "Bread"};
        Sale result = salesPerformer.perform(args);
        Double subtotal = result.getSubtotal();
        Double rebate = result.getRebate();
        double total = result.getTotal();

// Expected
        Map<String, Product> productMap = MapUtil.createMap(salesPerformer.getProductHandler().getAll());
        Product apples = productMap.get("Apples");
        Product bread1 = productMap.get("Bread");
        Product soup1 = productMap.get("Soup");
        Product soup2 = productMap.get("Soup");
        Product bread2 = productMap.get("Bread");
        double expectedSubtotal = LocaleUtil.preformatDecimals(
                apples.getPrice().getValue()
                + bread1.getPrice().getValue()
                + soup1.getPrice().getValue()
                + soup2.getPrice().getValue()
                + bread2.getPrice().getValue(),
                2);
        String expectedSubtotalAsString = LocaleUtil.preformatDecimalsAsString(expectedSubtotal, 2);
        double expectedRebate1 = LocaleUtil.preformatDecimals((apples.getPrice().getValue() * .1) * -1, 1);
        double expectedRebate2 = LocaleUtil.preformatDecimals(bread1.getPrice().getValue() / 2 * -1, 1);
        double expectedTotalRebate = LocaleUtil.preformatDecimals(expectedRebate1 + expectedRebate2, 1);
        double expectedTotal = LocaleUtil.preformatDecimals(expectedSubtotal + expectedRebate1 + expectedRebate2, 1);
        String expectedTotalAsString = LocaleUtil.preformatDecimalsAsString(expectedTotal, 2);
        String expectedOutput = "\nSubtotal: £" + expectedSubtotalAsString + "\n"
                + "Apples 10.0% x 1 off: " + (int) (expectedRebate1 * 100) + "p\n"
                + "Bread :: 2 Soup - get a Bread for half price x 1 off: " + (int) (expectedRebate2 * 100) + "p\n"
                + "Total: £" + expectedTotalAsString + "\n";

// Compare        
        Object[][] objectArray = new Object[][]{
            {subtotal, expectedSubtotal},
            {rebate, expectedTotalRebate},
            {total, expectedTotal},
            {result.toString(), expectedOutput}};
        verify(objectArray);
    }

    private void verify(Object[][] objectArray) {
        for (Object[] objects : objectArray) {
            assertThat(objects[0]).isEqualTo(objects[1]);
        }
    }

    @Test
    public void testNonFailingWithTwoBreadOfferTwoApples() {

// Actual
        String[] args = new String[]{"Apples", "Bread", "Soup", "Soup", "Bread", "Apples"};
        Sale result = salesPerformer.perform(args);
        Double subtotal = result.getSubtotal();
        Double rebate = result.getRebate();
        double total = result.getTotal();

// Expected
        Map<String, Product> productMap = MapUtil.createMap(salesPerformer.getProductHandler().getAll());
        Product apples1 = productMap.get("Apples");
        Product bread1 = productMap.get("Bread");
        Product soup1 = productMap.get("Soup");
        Product soup2 = productMap.get("Soup");
        Product bread2 = productMap.get("Bread");
        Product apples2 = productMap.get("Apples");
        double expectedSubtotal = LocaleUtil.preformatDecimals(
                apples1.getPrice().getValue()
                + bread1.getPrice().getValue()
                + soup1.getPrice().getValue()
                + soup2.getPrice().getValue()
                + bread2.getPrice().getValue()
                + apples1.getPrice().getValue(),
                2);
        String expectedSubtotalAsString = LocaleUtil.preformatDecimalsAsString(expectedSubtotal, 2);
        double expectedRebate1 = LocaleUtil.preformatDecimals(((apples1.getPrice().getValue()
                + apples2.getPrice().getValue()) * .1) * -1, 1);
        double expectedRebate2 = LocaleUtil.preformatDecimals(bread1.getPrice().getValue() / 2 * -1, 1);
        double expectedTotalRebate = LocaleUtil.preformatDecimals(expectedRebate1 + expectedRebate2, 1);
        double expectedTotal = LocaleUtil.preformatDecimals(expectedSubtotal + expectedRebate1 + expectedRebate2, 1);
        String expectedTotalAsString = LocaleUtil.preformatDecimalsAsString(expectedTotal, 2);
        String expectedOutput = "\nSubtotal: £" + expectedSubtotalAsString + "\n"
                + "Apples 10.0% x 2 off: " + (int) (expectedRebate1 * 100) + "p\n"
                + "Bread :: 2 Soup - get a Bread for half price x 1 off: " + (int) (expectedRebate2 * 100) + "p\n"
                + "Total: £" + expectedTotalAsString + "\n";

// Compare
        Object[][] objectArray = new Object[][]{
            {subtotal, expectedSubtotal},
            {rebate, expectedTotalRebate},
            {total, expectedTotal},
            {result.toString(), expectedOutput}};
        verify(objectArray);
    }

    @Test
    public void nonFailingTestWithOffer_soup() {
//Buy 2 tins of soup and get a loaf of bread for half price

        String[] args = new String[]{"Soup", "Soup", "Bread"};

        Sale result = salesPerformer.perform(args);

        Double subtotal = result.getSubtotal();
        Double rebate = result.getRebate();
        double total = result.getTotal();

        Map<String, Product> productMap = MapUtil.createMap(salesPerformer.getProductHandler().getAll());
        Product soup1 = productMap.get("Soup");
        Product soup2 = productMap.get("Soup");
        Product bread2 = productMap.get("Bread");
        double expectedSubtotal = LocaleUtil.preformatDecimals(
                soup1.getPrice().getValue()
                + soup2.getPrice().getValue()
                + bread2.getPrice().getValue(),
                2);
        String expectedSubtotalAsString = LocaleUtil.preformatDecimalsAsString(expectedSubtotal, 2);
        double expectedTotalRebate = LocaleUtil.preformatDecimals(bread2.getPrice().getValue() / 2 * -1, 1);
        double expectedTotal = LocaleUtil.preformatDecimals(expectedSubtotal + expectedTotalRebate, 1);
        String expectedTotalAsString = LocaleUtil.preformatDecimalsAsString(expectedTotal, 2);
        String expectedOutput = "\nSubtotal: £" + expectedSubtotalAsString + "\n"
                + "Bread :: 2 Soup - get a Bread for half price x 1 off: " + (int) (expectedTotalRebate * 100) + "p\n"
                + "Total: £" + expectedTotalAsString + "\n";

        Object[][] objectArray = new Object[][]{
            {subtotal, expectedSubtotal},
            {rebate, expectedTotalRebate},
            {total, expectedTotal},
            {result.toString(), expectedOutput}};
        verify(objectArray);
        System.out.println("" + result);
    }

    @Test
    public void nonFailingTestWithNoOffer() {

        String[] args = new String[]{"Butter", "Salt"};
        Sale result = salesPerformer.perform(args);

        Double subtotal = result.getSubtotal();
        Double rebate = result.getRebate();
        double total = result.getTotal();

        Map<String, Product> productMap = MapUtil.createMap(salesPerformer.getProductHandler().getAll());
        Product butter = productMap.get("Butter");
        Product salt = productMap.get("Salt");
        double expectedSubtotal = LocaleUtil.preformatDecimals(
                butter.getPrice().getValue()
                + salt.getPrice().getValue(),
                2);
        double expectedTotalRebate = 0;
        double expectedTotal = LocaleUtil.preformatDecimals(expectedSubtotal + expectedTotalRebate, 1);
        String expectedOutput = TestConstants.EXPECTED_OUTPUT_NO_OFFERS;
        
        Object[][] objectArray = new Object[][]{
            {subtotal, expectedSubtotal},
            {rebate, expectedTotalRebate},
            {total, expectedTotal},
            {result.toString(), expectedOutput}};
        verify(objectArray);
    }

    @Test(expected = NoResultException.class)
    public void failingTestWithNoOffer() {
        String[] args = new String[]{"ttt1", "Salt"};
        salesPerformer.perform(args);

        fail("Should never reach here ...");
    }
}
