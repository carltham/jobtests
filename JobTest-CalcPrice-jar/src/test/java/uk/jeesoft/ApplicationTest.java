/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft;

import javax.persistence.NoResultException;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import org.junit.Before;
import org.junit.Test;
import uk.jeesoft.db.Sale;
import uk.jeesoft.sales.TestConstants;

/**
 *
 * @author carl
 */
public class ApplicationTest {

    @Before
    public void setUp() {
    }

    @Test
    public void nonFailingTestMainWithOneOffer() throws IllegalAccessException, InstantiationException {
        String[] args = new String[]{"Apples", "Milk", "Bread"};
        Application.main(args);
    }

    @Test
    public void nonFailingTestMainWithNoOffer() throws IllegalAccessException, InstantiationException {
        String[] args = new String[]{"Butter", "Salt"};
        Application.main(args);
    }

    @Test(expected = IllegalArgumentException.class)
    public void failingTestMainWithNoSelectedProducts() throws IllegalAccessException, InstantiationException {
        String[] args = new String[]{};
        try {
            Application.main(args);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
            throw ex;
        }
    }

    @Test(expected = NoResultException.class)
    public void failingTestWithInvalidProduct() throws IllegalAccessException, InstantiationException {

        String[] args = new String[]{"ttt1", "Salt"};
        Application.main(args);
        fail("Should never reach here ...");
    }

    @Test
    public void nonFailingTestWithOneOffer() throws IllegalAccessException, InstantiationException {

        String[] args = new String[]{"Apples", "Milk", "Bread"};
        Application application = new Application();
        Sale result = application.execute(args);

        Double subtotal = result.getSubtotal();
        Double rebate = result.getRebate();
        double total = result.getTotal();

        assertThat(subtotal).isEqualTo(3.10);
        assertThat(rebate).isEqualTo(-0.10);
        assertThat(total).isEqualTo(3.00);
        assertThat(result.toString()).isEqualTo(TestConstants.EXPECTED_OUTPUT_WITH_OFFERS);
    }

    @Test
    public void nonFailingTestWithNoOffer() throws IllegalAccessException, InstantiationException {

        String[] args = new String[]{"Butter", "Salt"};
        Application application = new Application();
        Sale result = application.execute(args);

        Double subtotal = result.getSubtotal();
        Double rebate = result.getRebate();
        double total = result.getTotal();

        assertThat(subtotal).isEqualTo(1.30);
        assertThat(rebate).isEqualTo(0);
        assertThat(total).isEqualTo(1.30);
        assertThat(result.toString()).isEqualTo(TestConstants.EXPECTED_OUTPUT_NO_OFFERS);
    }

}
