/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft;

import javax.persistence.NoResultException;
import uk.jeesoft.db.Sale;
import uk.jeesoft.db.handlers.ProductHandler;
import uk.jeesoft.sale.SalesPerformer;

/**
 *
 * @author carl
 */
public class Application {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        Application application = new Application();
        if (args.length < 1) {
            listAllProducts("Usage : java -jar product1 product2 product3 ...", application);
            System.exit(-1);
        }

        try {
            Sale sale = application.execute(args);
            System.out.println("" + sale);
        } catch (NoResultException e) {
            listAllProducts("Minimum one wanted product was missing from listed products", application);
            System.exit(-1);
        }
        System.exit(0);
    }

    private static void listAllProducts(String text, Application application) throws IllegalAccessException, InstantiationException {
        String productsListString = application.listAll();
        String msg = "\n\n" + text + "\n\n" + productsListString;
        System.out.println(msg);
    }

    Sale execute(String[] args) throws IllegalAccessException, InstantiationException {
        SalesPerformer salesPerformer = new SalesPerformer();
        Sale sale = salesPerformer.perform(args);
        return sale;
    }

    private String listAll() throws IllegalAccessException, InstantiationException {
        ProductHandler productHandler = new ProductHandler();
        String productsListString = productHandler.listAll();
        productHandler.clearDB();
        return productsListString;
    }

}
