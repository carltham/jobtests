package uk.jeesoft.domain.handlers;

import uk.jeesoft.types.Currency;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import uk.jeesoft.domain.DataCarrier;
import uk.jeesoft.domain.builders.DataCarrierBuilder;
import uk.jeesoft.types.TradeType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author carl
 */
public class DataCarrierHandler {

    public void init() throws ParseException {
        DataCarrier dataCarrier = new DataCarrierBuilder()
                .withEntity("foo").withTradeType(TradeType.B)
                .withAgreedFx(0.50).withCurrency(Currency.SGP)
                .withInstructionDate(parseDate("01 Jan 2016")).withSettlementDate(parseDate("02 Jan 2016"))
                .withUnits(200).withPricePerUnit(100.25).build();
        // 							
    }

    private Date parseDate(String dateString) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        format.setLenient(false);
        Date date = format.parse(dateString);
        return date;
    }

}
