/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author carl
 */
package uk.jeesoft.domain;

import java.util.Date;
import uk.jeesoft.types.Currency;
import uk.jeesoft.types.TradeType;

public class DataCarrier {

    private Entity entity;
    private TradeType tradeType;
    private double agreedFx;
    private Currency currency;
    private Date instructionDate;
    private Date settlementDate;
    private int units;
    private double pricePerUnit;

    public void setEntity(Entity newEntity) {
        entity = newEntity;
    }

    public void setTradeType(TradeType tradeType) {
        tradeType = tradeType;
    }

    public void setAgreedFx(double newAgreedFx) {
        agreedFx = newAgreedFx;
    }

    public void setCurrency(Currency newCurrency) {
        currency = newCurrency;
    }

    public void setInstructionDate(Date newDate) {
        instructionDate = newDate;
    }

    public void setSettlementDate(Date newSettlementDate) {
        settlementDate = newSettlementDate;
    }

    public void setUnits(int newUnits) {
        units = newUnits;
    }

    public void setPricePerUnit(double newPricePerUnit) {
        pricePerUnit = newPricePerUnit;
    }

}
