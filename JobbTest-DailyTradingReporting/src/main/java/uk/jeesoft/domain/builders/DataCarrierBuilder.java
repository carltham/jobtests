/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.domain.builders;

import java.util.Date;
import uk.jeesoft.domain.DataCarrier;
import uk.jeesoft.types.Currency;
import uk.jeesoft.domain.Entity;
import uk.jeesoft.types.TradeType;

/**
 *
 * @author carl
 */
public class DataCarrierBuilder {

    private final DataCarrier dataCarrier;

    public DataCarrierBuilder() {
        dataCarrier = new DataCarrier();
    }

    public DataCarrierBuilder withEntity(String entityName) {
        dataCarrier.setEntity(new Entity(entityName));
        return this;
    }

    public DataCarrierBuilder withTradeType(TradeType tradeType) {
        dataCarrier.setTradeType(tradeType);
        return this;
    }

    public DataCarrierBuilder withAgreedFx(double agreedFx) {
        dataCarrier.setAgreedFx(agreedFx);
        return this;
    }

    public DataCarrierBuilder withCurrency(Currency currency) {
        dataCarrier.setCurrency(currency);
        return this;
    }

    public DataCarrierBuilder withInstructionDate(Date date) {
        dataCarrier.setInstructionDate(date);
        return this;
    }

    public DataCarrierBuilder withSettlementDate(Date date) {

        dataCarrier.setSettlementDate(date);
        return this;
    }

    public DataCarrierBuilder withUnits(int units) {

        dataCarrier.setUnits(units);
        return this;
    }

    public DataCarrierBuilder withPricePerUnit(double pricePerUnit) {
        dataCarrier.setPricePerUnit(pricePerUnit);
        return this;
    }

    public DataCarrier build() {
        return dataCarrier;
    }

}
